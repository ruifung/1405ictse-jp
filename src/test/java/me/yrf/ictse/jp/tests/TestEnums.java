package me.yrf.ictse.jp.tests;

import me.yrf.ictse.jp.api.auth.EnumPermission;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class TestEnums {
    @Test
    public void testPermEnum() {
        assertTrue(EnumUserAccessLevel.ADMINISTRATOR
                .hasPermission(EnumPermission.ADMINISTRATE));
        assertTrue(EnumUserAccessLevel.ADMINISTRATOR
                .hasAnyPermission(EnumPermission.ADMINISTRATE));
        assertFalse(EnumUserAccessLevel.ADMINISTRATOR
                .hasAllPermissions(EnumPermission.ADMINISTRATE, EnumPermission.ORDER));
    }
}
