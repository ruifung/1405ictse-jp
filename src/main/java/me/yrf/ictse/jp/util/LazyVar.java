package me.yrf.ictse.jp.util;

import java.util.function.Supplier;

public class LazyVar<T> extends LazyVal<T> {
    public LazyVar(Supplier<T> supplier) {
        super(supplier);
    }

    public void set(T value) {
        if(supplier != null) supplier = null;
        this.value = value;
    }
}
