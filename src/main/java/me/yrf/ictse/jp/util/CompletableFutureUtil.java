package me.yrf.ictse.jp.util;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class CompletableFutureUtil {

    /**
     * Actually get the list of results from a list of futures .
     * reference: http://www.nurkiewicz.com/2013/05/java-8-completablefuture-in-action.html
     *
     * @param futures List of CompletableFutures.
     * @param <T> Type of value used.
     * @return A CompletableFuture of type List<T>.
     */
    public static <T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> futures) {
        java.util.concurrent.CompletableFuture<Void> allDoneFuture =
                CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
        return allDoneFuture.thenApply(v -> futures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList())
        );
    }

    /**
     * Version of getNow that lazy loads the alternative value.
     * reference: http://www.nurkiewicz.com/2013/05/java-8-completablefuture-in-action.html
     *
     * @param future The CompletableFuture
     * @param valueIfAbsent A Supplier to supply a value if absent.
     * @param <T> Type of returned value.
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static <T> T getNow(
            CompletableFuture<T> future,
            Supplier<T> valueIfAbsent) throws ExecutionException, InterruptedException {
        if (future.isDone()) {
            return future.get();
        } else {
            return valueIfAbsent.get();
        }
    }
}
