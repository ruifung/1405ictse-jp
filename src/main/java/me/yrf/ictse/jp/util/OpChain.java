package me.yrf.ictse.jp.util;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * OPeration Chain
 *
 * Allows chaining operations on a single object using lambda expressions.
 * @param <T>
 */
public class OpChain<T> {
    private final T obj;

    private OpChain(T obj) {
        this.obj = obj;
    }

    public static <T> OpChain<T> beginChain(T obj) {
        return new OpChain<>(obj);
    }

    public static <T> OpChain<T> chain(T obj, Consumer<T> op) {
        op.accept(obj);
        return new OpChain<>(obj);
    }

    public OpChain<T> chain(Consumer<T> op) {
        op.accept(obj);
        return this;
    }

    public <R> OpChain<R> map(Function<T, R> op) {
        return beginChain(op.apply(obj));
    }

    public T endWith(Consumer<T> op) {
        op.accept(obj);
        return obj;
    }

    public T end() {
        return obj;
    }
}
