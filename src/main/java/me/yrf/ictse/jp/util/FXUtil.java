package me.yrf.ictse.jp.util;

import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import me.yrf.ictse.jp.api.data.entities.IIdEntity;

public class FXUtil {
    public static <T extends Node> T cssClass(T node, String... classes) {
        node.getStyleClass().addAll(classes);
        return node;
    }

    public static <T extends Node> T cssId(T node, String id) {
        node.setId(id);
        return node;
    }

    public static <T extends Node> T cssIdClass(T node, String id,
                                                String... classes) {
        return cssClass(cssId(node, id), classes);
    }

    public static <T extends Node> void anchorMax(T node) {
        AnchorPane.setTopAnchor(node, 0.0);
        AnchorPane.setBottomAnchor(node, 0.0);
        AnchorPane.setLeftAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.0);
    }

    public static ObservableValue<Number> idBeanProperty(TableColumn.CellDataFeatures<? extends IIdEntity, Number> cd) {
        try {
            return new ReadOnlyJavaBeanIntegerPropertyBuilder()
                    .bean(cd.getValue()).name("id").build();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }
}
