package me.yrf.ictse.jp.util;

import java.util.function.Supplier;

/**
 * Lazy values.
 * Only performs initalization when accessed.
 * Can, and WILL return null if value is set to null.
 * @param <T>
 */
public class LazyVal<T> {
    protected T value;
    protected Supplier<T> supplier;

    public LazyVal(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public T get() {
        if (value == null && supplier != null) {
            value = supplier.get();
            supplier = null;
        }
        return value;
    }
}
