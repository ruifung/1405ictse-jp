package me.yrf.ictse.jp.util;

import java.util.Arrays;
import java.util.function.Consumer;

public class Util {

    @SafeVarargs
    public static <T> void runWith(final T obj, final Consumer<T>... ops) {
        Arrays.asList(ops).forEach(c -> c.accept(obj));
    }

    @SafeVarargs
    public static <T> void runWithAsync(final T obj, final Consumer<T>... ops) {
        Arrays.asList(ops).parallelStream().forEach(c -> c.accept(obj));
    }
}
