package me.yrf.ictse.jp.gui;

import javafx.scene.Parent;
import javafx.scene.Scene;

import java.util.Arrays;

public class StyleRegistry {
    public static String[] defaultStylesheets = {
            "BaseStyles.css",
            "LoginScreen.css"
    };

    public static Scene applyDefaultStyles(Scene scene) {
        applyStyles(scene, defaultStylesheets);
        return scene;
    }

    public static <T extends Parent> T applyDefaultStyles(T node) {
        applyStyles(node, defaultStylesheets);
        return node;
    }

    public static Scene applyStyles(Scene scene, String... filenames) {
        Arrays.asList(filenames).stream()
                .map(StyleRegistry::mapCssFilename)
                .forEach(scene.getStylesheets()::add);
        return scene;
    }

    public static <T extends Parent> T applyStyles(T node, String... filenames) {
        Arrays.asList(filenames).stream()
                .map(StyleRegistry::mapCssFilename)
                .forEach(node.getStylesheets()::add);
        return node;
    }

    private static String mapCssFilename(String fileName) {
        return StyleRegistry.class
                .getResource("/gui/styles/" + fileName)
                .toExternalForm();
    }
}
