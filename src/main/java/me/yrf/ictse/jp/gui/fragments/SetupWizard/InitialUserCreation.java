package me.yrf.ictse.jp.gui.fragments.SetupWizard;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.data.inmem.users.InMemUser;
import me.yrf.ictse.jp.gui.activities.LoginActivity;
import me.yrf.ictse.jp.gui.activities.SetupWizard;
import me.yrf.ictse.jp.util.FXUtil;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;

public class InitialUserCreation extends Fragment {
    private final SetupWizard host;
    @FXML
    private Node base;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField fullName;

    public InitialUserCreation(SetupWizard host) {
        this.host = host;
    }

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        fxmlLoader.setLocation(getClass().getResource("/gui/fragments/SetupWizard/InitialUserCreation.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FXUtil.anchorMax(base);
    }

    @FXML
    private void onBackPressed() {
        this.host.onFragmentBackPressed();
    }

    @FXML
    private void onCreateUser() {
        if(username.getText().isEmpty() || password.getText().isEmpty()) {
            val root = (Pane) this.getScene().getRoot();
            val btn = new MaterialButton();
            btn.setText("OK");
            val dialog = EntryPoint.errorDialog("Blank User",
                    new RuntimeException("Username / password cannot be blank!"), root, btn);
            if (dialog != null)
                btn.setOnAction(e -> dialog.close());
            return;
        }

        val userMan = Registry.getInstance().getDbManager().getUserManager();
        val user = new InMemUser(username.getText());
        user.setAccessLevel(EnumUserAccessLevel.ADMINISTRATOR);
        user.setFullName(fullName.getText());

        val future = userMan.add(user, password.getText());
        future.whenComplete((v, e) -> {
            if (e == null)
                Platform.runLater(() -> host.startActivity(LoginActivity.class));
            else//TODO: Show message dialog?
                e.printStackTrace();
        });

    }
}
