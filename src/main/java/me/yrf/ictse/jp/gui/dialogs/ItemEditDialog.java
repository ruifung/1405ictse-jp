package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.StringConverter;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.Procedure;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.api.data.entities.ISupplier;

import java.io.IOException;

public class ItemEditDialog extends JFXDialogController {

    private final int itemId;
    @FXML
    Label title;
    @FXML
    TextField name;
    @FXML
    TextArea desc;
    @FXML
    ComboBox<ISupplier> supplier;
    private Procedure saveHandler;
    private boolean isEditing;

    public ItemEditDialog(Pane container, JFXDialog.DialogTransition transition, IItem item) throws IOException {
        val loader = new FXMLLoader(this.getClass().getResource("/gui/dialogs/ItemEditDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();

        this.getDialog().setDialogContainer(container);
        this.getDialog().setContent(content);
        this.setOverlayCloseable(false);
        this.getDialog().setTransitionType(transition);

        supplier.setConverter(new StringConverter<ISupplier>() {
            @Override
            public String toString(ISupplier object) {
                return String.format("#%s %s", object.getId(), object.getName());
            }

            @Override
            public ISupplier fromString(String string) {
                return null;
            }
        });
        val supplierMan = Registry.getInstance().getDbManager().getSupplierManager();
        supplierMan.count()
                .thenCompose(c -> supplierMan.getMany(0, c))
                .whenComplete((v, e) -> {
                    if (v != null) {
                        supplier.getItems().clear();
                        supplier.getItems().addAll(v);
                    }
                });

        if (item != null) {
            isEditing = true;
            itemId = item.getId();
            title.setText(String.format("Editing Item: #%s", item.getId()));
            name.setText(item.getName());
            desc.setText(item.getDesc());
            supplierMan.get(item.getSupplierId())
                    .whenComplete((v, e) -> {
                        if (v != null)
                            supplier.getSelectionModel().select(v);
                    });
        } else {
            isEditing = false;
            itemId = -1;
            title.setText("Add New Item");
        }
    }

    @FXML
    private void onSave(Event e) {
        if (this.saveHandler != null)
            this.saveHandler.invoke();
    }

    public Procedure getSaveHandler() {
        return saveHandler;
    }

    public void setSaveHandler(Procedure saveHandler) {
        this.saveHandler = saveHandler;
    }

    public boolean isEditing() {
        return isEditing;
    }

    public int getItemId() {
        return itemId;
    }

    public String getName() {
        return name.getText();
    }

    public String getDesc() {
        return desc.getText();
    }

    public ISupplier getSupplier() {
        return supplier.getSelectionModel().getSelectedItem();
    }

    @FXML
    @Override
    public void close() {
        super.close();
    }
}
