package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.event.ActionEvent;
import javafx.scene.layout.Pane;
import lombok.val;
import me.yrf.ictse.jp.api.Procedure;
import org.kairos.components.MaterialButton;

import java.io.IOException;

public class PromptDialog extends MessageDialog {

    private Procedure onYes, onNo;

    public PromptDialog(Pane container, JFXDialog.DialogTransition transition) throws IOException {
        super(container, transition);

        val btnYes = new MaterialButton();
        btnYes.setOnAction(this::onBtnYes);
        btnYes.setText("Yes");
        val btnNo = new MaterialButton();
        btnNo.setText("No");
        btnNo.setOnAction(this::onBtnNo);
        btnNo.setFlated(true);

        getButtonBox().getChildren().addAll(btnNo, btnYes);
    }

    private void onBtnYes(ActionEvent e) {
        if (onYes != null)
            onYes.invoke();
    }

    private void onBtnNo(ActionEvent e) {
        if (onNo != null)
            onNo.invoke();
    }

    public Procedure getOnYes() {
        return onYes;
    }

    public void setOnYes(Procedure onYes) {
        this.onYes = onYes;
    }

    public Procedure getOnNo() {
        return onNo;
    }

    public void setOnNo(Procedure onNo) {
        this.onNo = onNo;
    }
}
