package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import lombok.val;

import java.io.IOException;

public class MessageDialog extends JFXDialogController {

    @FXML
    private VBox contentPane;
    @FXML
    private TextFlow contentFlow;
    @FXML
    private Label title;
    @FXML
    private Text contentText;
    @FXML
    private HBox buttonBox;
    @FXML
    private HBox topBox;

    public MessageDialog(Pane container, JFXDialog.DialogTransition transition) throws IOException {
        val loader = new FXMLLoader(MessageDialog.class.getResource("/gui/dialogs/MessageDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();
        val dialog = getDialog();
        dialog.setDialogContainer(container);
        dialog.setContent(content);
        dialog.setTransitionType(transition);
        dialog.show();
        dialog.close();

    }

    public String getTitle() {
        return title.getText();
    }

    public void setTitle(String title) {
        if (title != null)
            this.title.setText(title);
    }

    public String getText() {
        return contentText.getText();
    }

    public void setText(String text) {
        contentText.setText(text);
    }

    public HBox getButtonBox() {
        return buttonBox;
    }

    public void setIcon(Image image) {
        if (image == null) {
            title.setGraphic(null);
        } else {
            val icon = new ImageView(image);
            icon.setPreserveRatio(true);
            icon.setSmooth(true);
            icon.setFitHeight(64);
            title.setGraphic(icon);
            title.setGraphicTextGap(5);
        }
    }

    public void show() {
        getDialog().show();
    }

    public void show(String title, String content) {
        setTitle(title);
        setText(content);
        getDialog().show();
    }
}

