package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import lombok.val;

import java.io.IOException;

public class LoadingDialog extends JFXDialogController {

    private final static String defaultText = "Loading";
    @FXML
    private Label label;

    public LoadingDialog(Pane container, JFXDialog.DialogTransition transition) throws IOException {
        val loader = new FXMLLoader(LoadingDialog.class.getResource("/gui/dialogs/LoadingDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();
        getDialog().setDialogContainer(container);
        getDialog().setContent(content);
        getDialog().setTransitionType(transition);
    }

    public void show() {
        show(null);
    }

    public void show(String text) {
        label.setText(text != null ? text : defaultText);
        getDialog().show();
    }


}
