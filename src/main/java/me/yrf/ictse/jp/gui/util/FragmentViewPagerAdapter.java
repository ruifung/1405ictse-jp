package me.yrf.ictse.jp.gui.util;

import javafx.util.Pair;
import org.kairos.FragmentStatePagerAdapter;
import org.kairos.core.Fragment;
import org.kairos.core.FragmentManager;

import java.util.ArrayList;


public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<Pair<String, ? extends Fragment>> fragments = new ArrayList<>();

    public FragmentViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i).getValue();
    }

    @Override
    public String getPageTitle(int i) {
        return fragments.get(i).getKey();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public <T extends Fragment> void add(String title, T fragment) {
        fragments.add(new Pair<>(title, fragment));
    }
}
