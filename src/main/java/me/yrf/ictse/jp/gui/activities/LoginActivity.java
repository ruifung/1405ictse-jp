package me.yrf.ictse.jp.gui.activities;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.gui.dialogs.LoginDialog;
import me.yrf.ictse.jp.gui.dialogs.MessageDialog;
import org.kairos.core.Activity;

import java.io.IOException;

public class LoginActivity extends Activity {

    @FXML
    private StackPane root;

    private MessageDialog errorDialog;

    @Override
    public void onCreate() {
        super.onCreate();
        this.setContentView(getClass().getResource("/gui/activities/LoginActivity.fxml"));
        try {
            val login = new LoginDialog(root, JFXDialog.DialogTransition.CENTER);
            login.setOverlayCloseable(false);
            login.setCancelHandler(Platform::exit);
            login.setLoginHandler(this::handleLogin);
            login.show();
            this.root.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
                if (e.getCode().equals(KeyCode.ENTER)) {
                    if (errorDialog != null) {
                        errorDialog.close();
                        errorDialog = null;
                    } else
                        login.onLogin();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleLogin(String username, String password) {
        val auth = Registry.getInstance().getDbManager().getUserManager()
                .getAuthenticator().authenticate(username, password);
        if (auth.isAuthenticated() && auth.getUser().isPresent()) {
            Registry.getInstance().setCurrentUser(auth.getUser().get());
            this.startActivity(MainActivity.class);
        } else {
            try {
                val dialog = new MessageDialog(root, JFXDialog.DialogTransition.CENTER);
                dialog.setTitle("Authentication Failure");
                dialog.setText("Invalid username or password");

                val okBtn = new JFXButton("OK");
                okBtn.setOnAction(e -> dialog.close());
                dialog.getButtonBox().getChildren().add(okBtn);
                this.errorDialog = dialog;
                dialog.show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

