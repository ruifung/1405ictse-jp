package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXDialog;
import javafx.application.Platform;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.data.ISupplierManager;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.api.data.entities.ISupplier;
import me.yrf.ictse.jp.data.inmem.suppliers.InMemSupplier;
import me.yrf.ictse.jp.gui.dialogs.SupplierEditDialog;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class SupplierFragment extends Fragment {
    @FXML
    private TableView<ISupplier> table;
    private ISupplierManager supMan = Registry.getInstance().getDbManager().getSupplierManager();
    private ObservableList<ISupplier> items = FXCollections.observableList(new LinkedList<>());

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        try {
            fxmlLoader.setLocation(getClass().getResource("/gui/fragments/SupplierManagement.fxml"));
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        initializeTableView();
    }

    @SuppressWarnings("unchecked")
    private void initializeTableView() {
        val col1 = new TableColumn<ISupplier, Number>("Supplier ID");
        col1.setCellValueFactory(cd -> {
            try {
                return ReadOnlyJavaBeanIntegerPropertyBuilder.create()
                        .bean(cd.getValue()).name("id").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col2 = new TableColumn<ISupplier, String>("Name");
        col2.setCellValueFactory(cd -> {
            try {
                return JavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("name").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col3 = new TableColumn<ISupplier, String>("Email");
        col3.setCellValueFactory(cd -> {
            try {
                return JavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("email").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col4 = new TableColumn<ISupplier, String>("Phone");
        col4.setCellValueFactory(cd -> {
            try {
                return JavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("phone").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col5 = new TableColumn<ISupplier, String>("Address");
        col5.setCellValueFactory(cd -> {
            try {
                return JavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("address").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });

        table.setItems(items);
        table.getColumns().setAll(col1, col2, col3, col4, col5);
        table.setEditable(false);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    @FXML
    private void addItem() {
        try {
            val root = (StackPane) this.getScene().getRoot();
            val dialog = new SupplierEditDialog(root, JFXDialog.DialogTransition.CENTER, null);
            dialog.setSaveHandler(() -> {
                if (!dialog.isEditing()) {
                    ISupplier item = new InMemSupplier();
                    item.setAddress(dialog.getAddress());
                    item.setName(dialog.getName());
                    item.setEmail(dialog.getEmail());
                    item.setPhone(dialog.getPhone());

                    CompletableFuture<ISupplier> retVal = supMan.add(item);
                    retVal.whenComplete((v, e) -> {
                        if (v != null)
                            Platform.runLater(() -> items.add(v));
                    });
                }
                dialog.close();
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void editItem() {
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        try {
            val root = (StackPane) this.getScene().getRoot();
            val editedItem = table.getSelectionModel().getSelectedItem();
            val dialog = new SupplierEditDialog(root, JFXDialog.DialogTransition.CENTER, editedItem);

            dialog.setSaveHandler(() -> {
                if (editedItem != null) {
                    editedItem.setPhone(dialog.getPhone());
                    editedItem.setName(dialog.getName());
                    editedItem.setEmail(dialog.getEmail());
                    editedItem.setAddress(dialog.getAddress());

                    supMan.update(editedItem).whenComplete((v2, e2) -> {
                        if (v2 != null)
                            items.set(items.indexOf(editedItem), v2);
                    });
                    dialog.close();
                }
            });

            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    //TODO: Parallel item dependency checks.
    private void removeItem() {
        val itemMan = Registry.getInstance().getDbManager().getItemManager();
        try {
            val items = itemMan.count().thenCompose(c -> itemMan.getMany(0, c)).get();
            CompletableFuture.allOf(
                    table.getSelectionModel()
                            .getSelectedItems()
                            .stream()
                            .filter(i -> {
                                for (IItem it : items) {
                                    if (it.getSupplierId() == i.getId())
                                        return false;
                                }
                                return true;
                            })
                            .map(u -> {
                                CompletableFuture<ISupplier> future = supMan.remove(u.getId());
                                return future.thenAccept(v -> this.items.remove(u));
                            })
                            .toArray(CompletableFuture[]::new)
            ).whenComplete((v, e) -> {
                if (e == null)
                    Platform.runLater(this::refreshView);
            });
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            val root = (Pane) this.getScene().getRoot();
            val btn = new MaterialButton();
            val dialog = EntryPoint.errorDialog("Datastore Error",
                    new RuntimeException("Cannot access items"), root, btn);
            btn.setText("OK");
            if (dialog != null)
                btn.setOnAction(ev -> dialog.close());
            return;
        }
    }

    private void refreshView() {
        ForkJoinPool.commonPool().submit(() -> {
            supMan.count()
                    .thenCompose(c -> supMan.getMany(0, c))
                    .thenAccept(v -> Platform.runLater(() -> this.items.setAll(v)));
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshView();
    }
}
