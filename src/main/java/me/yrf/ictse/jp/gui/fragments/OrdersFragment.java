package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXDialog;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanStringPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.EnumPermission;
import me.yrf.ictse.jp.api.data.IOrderManager;
import me.yrf.ictse.jp.api.data.entities.IOrder;
import me.yrf.ictse.jp.gui.dialogs.OrderDialog;
import me.yrf.ictse.jp.gui.dialogs.PromptDialog;
import me.yrf.ictse.jp.util.FXUtil;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.LinkedList;

public class OrdersFragment extends Fragment {
    @FXML
    private TableView<IOrder> table;
    @FXML
    private HBox btnBar;
    @FXML
    private MaterialButton removeBtn;

    private ObservableList<IOrder> ords = FXCollections.observableList(new LinkedList<>());
    private IOrderManager ordMan = Registry.getInstance().getDbManager().getOrderManager();

    @Override
    @SuppressWarnings("unchecked")
    public void onCreateView(FXMLLoader fxmlLoader) {
        try {
            fxmlLoader.setLocation(getClass().getResource("/gui/fragments/OrdersFragment.fxml"));
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!Registry.getInstance().getCurrentUser().get()
                .getAccessLevel().hasPermission(EnumPermission.ORDER)) {
            btnBar.setVisible(false);
            btnBar.setManaged(false);
        }

        table.setItems(ords);
        ordMan.count()
                .thenCompose(c -> ordMan.getMany(0, c))
                .whenComplete((v, o0o) -> {
                    if (o0o == null) {
                        ords.clear();
                        ords.addAll(v);
                    }
                });

        val col1 = new TableColumn<IOrder, Number>("Order ID");
        val col2 = new TableColumn<IOrder, LocalDate>("Due By");
        val col3 = new TableColumn<IOrder, String>("Created By");
        val col4 = new TableColumn<IOrder, String>("Approved By");
        val col5 = new TableColumn<IOrder, LocalDate>("Ordered On");
        col1.setCellValueFactory(FXUtil::idBeanProperty);
        col2.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanObjectPropertyBuilder<LocalDate>()
                        .bean(cd.getValue()).name("dueDate").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col3.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanStringPropertyBuilder()
                        .bean(cd.getValue().getCreator()).name("username").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col4.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanStringPropertyBuilder()
                        .bean(cd.getValue().getApprover()).name("username").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col5.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanObjectPropertyBuilder<LocalDate>()
                        .bean(cd.getValue()).name("orderedOn").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        table.getColumns().setAll(col1, col2, col3, col4, col5);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(ords);

        table.setRowFactory(tv -> {
            TableRow<IOrder> row = new TableRow<>();
            row.setOnMouseClicked(e -> {
                if (row.isEmpty())
                    return;
                if (e.getClickCount() == 2) {
                    try {
                        OrderDialog dialog = new OrderDialog((Pane) this.getScene().getRoot(),
                                JFXDialog.DialogTransition.CENTER, row.getItem());
                        dialog.show();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            return row;
        });
    }

    @FXML
    private void delOrder() {
        try {
            val toDelete = table.getSelectionModel().getSelectedItems();
            val root = (Pane) this.getScene().getRoot();
            val dialog = new PromptDialog(root, JFXDialog.DialogTransition.CENTER);
            dialog.setTitle("Confirm deletion");
            dialog.setText(String.format("Are you sure you want to delete these %s orders?", toDelete.size()));
            dialog.setOnNo(dialog::close);
            dialog.setOnYes(() -> {
                toDelete.stream().forEach(i -> ordMan.remove(i.getId()).thenAccept(r -> ords.remove(i)));
                dialog.close();
            });
            dialog.show();
        } catch (
                IOException e
                )

        {
            e.printStackTrace();
        }

    }

    @FXML
    private void setOrdered() {
        try {
            val root = (Pane) this.getScene().getRoot();
            val dialog = new PromptDialog(root, JFXDialog.DialogTransition.CENTER);
            val sel = table.getSelectionModel().getSelectedItems();
            dialog.setTitle("Confirm Mark as Ordered");
            dialog.setText(String.format("Are you sure you want to mark %s orders as ordered?", sel.size()));
            dialog.setOnNo(dialog::close);
            dialog.setOnYes(() -> {
                sel.stream()
                        .filter(i -> i.getOrderedOn() == null)
                        .forEach(i -> {
                            i.setOrderedOn(ZonedDateTime.now());
                            ordMan.update(i).thenAccept(u -> ords.set(ords.indexOf(i), u));
                        });
                dialog.close();
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
