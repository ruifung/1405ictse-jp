package me.yrf.ictse.jp.gui.activities;

import javafx.fxml.FXML;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.gui.fragments.SetupWizard.WelcomeScreen;
import org.kairos.core.Activity;
import org.kairos.layouts.ViewPager;

public class SetupWizard extends Activity {

    @FXML
    private ViewPager viewPager;

    @Override
    public void onCreate() {
        super.onCreate();
        this.setContentView(getClass().getResource("/gui/activities/SetupWizard.fxml"));
        val frags = getFragmentManager().beginTransaction();
        frags.add("content", new WelcomeScreen(this));
        frags.commit();
    }

    @Override
    public void onBackPressed() {

    }

    public void onFragmentBackPressed() {
        EntryPoint.exit(0);
    }
}
