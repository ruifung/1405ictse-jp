package me.yrf.ictse.jp.gui.fragments.SetupWizard;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.gui.activities.SetupWizard;
import me.yrf.ictse.jp.util.FXUtil;
import org.kairos.core.Fragment;

import java.io.IOException;

public class WelcomeScreen extends Fragment {

    private final SetupWizard parent;
    @FXML
    Node base;

    public WelcomeScreen(SetupWizard parent) {
        this.parent = parent;
    }

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        fxmlLoader.setLocation(getClass().getResource("/gui/fragments/SetupWizard/WelcomeScreen.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FXUtil.anchorMax(base);
    }

    @FXML
    private void onNextBtn(Event e) {
        val frags = this.getFragmentManager().beginTransaction();
        frags.replace("content", new InitialUserCreation(parent));
        frags.commit();
    }

    @FXML
    private void exit() {
        EntryPoint.exit(0);
    }
}
