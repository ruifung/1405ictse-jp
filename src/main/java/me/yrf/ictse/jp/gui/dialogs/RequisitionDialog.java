package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.adapter.JavaBeanIntegerPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.converter.NumberStringConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.api.data.entities.IRequisition;
import me.yrf.ictse.jp.data.inmem.requisitions.InMemRequisition;
import org.kairos.components.MaterialButton;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class RequisitionDialog extends JFXDialogController {

    private final int itemId;
    @FXML
    private Label title;
    @FXML
    private TextField creator;
    @FXML
    private JFXDatePicker dueDate;
    @FXML
    private TableView<Item> items;

    @FXML
    private MaterialButton closeBtn, saveBtn;

    private Mode mode;
    private IRequisition requisition;
    private Consumer<IRequisition> saveCallback;

    @SuppressWarnings("unchecked")
    private RequisitionDialog(Pane container, JFXDialog.DialogTransition transition, int id) throws IOException {
        val loader = new FXMLLoader(this.getClass().getResource("/gui/dialogs/RequisitionDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();

        this.getDialog().setDialogContainer(container);
        this.getDialog().setContent(content);
        this.setOverlayCloseable(false);
        this.getDialog().setTransitionType(transition);
        this.itemId = id;

        items.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        items.setEditable(true);
        val col1 = new TableColumn<Item, String>("Item");
        col1.setEditable(false);
        col1.setCellValueFactory(cd -> new SimpleStringProperty(cd.getValue().getItem().getName()));
        val col2 = new TableColumn<Item, Number>("Amount");
        col2.setEditable(true);
        col2.setCellValueFactory(cd -> {
            try {
                return new JavaBeanIntegerPropertyBuilder()
                        .bean(cd.getValue()).name("amount").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col2.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
        items.getColumns().setAll(col1, col2);

        dueDate.valueProperty().addListener((o, ov, nv) -> {
            if(nv != null && nv.isBefore(LocalDate.now())) {
                dueDate.setValue(ov);
                return;
            }
            if(nv != null)
                saveBtn.setDisable(false);
            else
                saveBtn.setDisable(true);
        });
    }

    public RequisitionDialog(Pane container, JFXDialog.DialogTransition transition,
                             @NonNull List<IItem> items) throws IOException {
        this(container, transition, -1);
        mode = Mode.CREATING;
        creator.setText(Registry.getInstance().getCurrentUser().get().getUsername());
        title.setText("Create New Item Requisition");

        this.saveBtn.setDisable(true);
        this.saveBtn.setText("Add");
        this.items.setItems(items.stream().map(i -> new Item(i, 0))
                .collect(Collectors.collectingAndThen(Collectors.toList(), FXCollections::observableList)));
    }

    public RequisitionDialog(Pane container, JFXDialog.DialogTransition transition,
                             @NonNull IRequisition requisition, boolean viewOnly) throws IOException {
        this(container, transition, requisition.getId());
        creator.setText(requisition.getCreator().getUsername());
        this.requisition = requisition;
        if (viewOnly) {
            mode = Mode.VIEWING;
            title.setText(String.format("View Requisition: #%s", requisition.getId()));
            setOverlayCloseable(true);
            dueDate.setEditable(false);
            dueDate.valueProperty().addListener((o, ov, nv) -> {
                if (!nv.equals(requisition.getDueDate()))
                    dueDate.setValue(requisition.getDueDate());
            });
            items.setEditable(false);
            saveBtn.setManaged(false);
            saveBtn.setVisible(false);
            closeBtn.setFlated(false);
            closeBtn.setText("Close");
        } else {
            mode = Mode.EDITING;
            title.setText(String.format("Edit Requisition: #%s", requisition.getId()));
            items.setEditable(true);
            dueDate.setEditable(true);
        }
        this.dueDate.setValue(requisition.getDueDate());
        this.items.setItems(requisition.getItems().entrySet().stream()
                .map(e -> new Item(e.getKey(), e.getValue()))
                .collect(Collectors.collectingAndThen(Collectors.toList(), FXCollections::observableList)));
    }

    @FXML
    private void onSave(Event e) {
        val reqMan = Registry.getInstance().getDbManager().getRequisitionManager();
        switch (mode) {
            case VIEWING:
                return; //Because a VIEWER shouldn't save anything.
            case EDITING:
                val reqItems = requisition.getItems();
                reqItems.clear();
                requisition.setDueDate(dueDate.getValue());
                items.getItems().stream()
                        .forEach(i -> reqItems.put(i.getItem(), i.getAmount()));
                if (reqItems.containsValue(0)) {
                    errDialog("Requisition 0 Items","Cannot place requisition for 0 items.");
                    return;
                }
                reqMan.update(requisition).thenAccept(r -> {
                    if(saveCallback!= null)
                        saveCallback.accept(r);
                });
                close();
                break;
            case CREATING:
                requisition = new InMemRequisition();
                requisition.setCreator(Registry.getInstance().getCurrentUser().get());
                requisition.setDueDate(dueDate.getValue());
                val reqItems2 = new HashMap<IItem, Integer>();
                items.getItems().stream()
                        .forEach(i -> reqItems2.put(i.getItem(), i.getAmount()));
                if (reqItems2.containsValue(0)) {
                    errDialog("Requisition 0 Items","Cannot place requisition for 0 items.");
                    return;
                }
                requisition.setItems(reqItems2);
                reqMan.add(requisition).thenAccept(r -> {
                    if(saveCallback!= null)
                        saveCallback.accept(r);
                });
                close();
                break;
        }
    }

    @FXML
    private void onAddBtn(Event e) {

    }

    @FXML
    private void onMinusBtn(Event e) {

    }

    @FXML
    @Override
    public void close() {
        super.close();
    }

    public Consumer<IRequisition> getSaveCallback() {
        return saveCallback;
    }

    public void setSaveCallback(Consumer<IRequisition> saveCallback) {
        this.saveCallback = saveCallback;
    }

    public enum Mode {
        VIEWING, EDITING, CREATING
    }

    @Data
    @AllArgsConstructor
    public static class Item {
        private IItem item;
        private int amount;
    }
}
