package me.yrf.ictse.jp.gui.activities;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.EnumPermission;
import me.yrf.ictse.jp.gui.controls.DrawerLayoutEx;
import me.yrf.ictse.jp.gui.controls.ToolbarEx;
import me.yrf.ictse.jp.gui.fragments.*;
import me.yrf.ictse.jp.gui.util.FragmentViewPagerAdapter;
import org.kairos.core.Activity;
import org.kairos.core.Fragment;
import org.kairos.layouts.DrawerLayout;
import org.kairos.layouts.ViewPager;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class MainActivity extends Activity {

    @FXML
    private DrawerLayoutEx drawer;
    @FXML
    private ToolbarEx toolbar;
    @FXML
    private StackPane nav;
    @FXML
    private ViewPager viewPager;

    private FragmentViewPagerAdapter pagerAdapter;

    private List<ViewItem> menuItems = new LinkedList<>();
    private Map<String, Integer> itemMap = new HashMap<>();

    {
        val user = Registry.getInstance().getCurrentUser().get();
        if (user.getAccessLevel().hasPermission(EnumPermission.ADMINISTRATE))
            menuItems.add(new ViewItem("Users", UserManagementFragment.class));
        if (user.getAccessLevel().hasPermission(EnumPermission.SUPPLIERS))
            menuItems.add(new ViewItem("Suppliers", SupplierFragment.class));
        if (user.getAccessLevel().hasPermission(EnumPermission.REQUISITION)) {
            menuItems.add(new ViewItem("Items", ItemManagementFragment.class));
            menuItems.add(new ViewItem("Requisitions", RequisitionsFragment.class));
            menuItems.add(new ViewItem("Orders", OrdersFragment.class));
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.setContentView(getClass().getResource("/gui/activities/MainActivity.fxml"));

        toolbar.setTitle("POPS System");
        toolbar.setDisplayShowHomeEnabled(true);
        toolbar.setOnHomeButtonAction(e -> this.toggleDrawer());
        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerClosed(Node node) {
                toolbar.setHomeVisible(true);
            }

            @Override
            public void onDrawerOpened(Node node) {
                toolbar.setHomeVisible(false);
            }
        });
        setActionBar(toolbar);

        //val searchField = new TextField();
        //toolbar.getTopRightPlace().getChildren().add(searchField);

        val navpane = new NavPaneFragment(str -> viewPager.setCurrentItem(itemMap.get(str)));
        val trans = getFragmentManager().beginTransaction();
        trans.add("nav", navpane);
        trans.commit();

        pagerAdapter = new FragmentViewPagerAdapter(getFragmentManager());

        for (int i = 0; i < menuItems.size(); i++) {
            val item = menuItems.get(i);
            itemMap.put(item.getDisplayName(), i);
            navpane.listView.getItems().add(item.getDisplayName());
            try {
                pagerAdapter.add(item.getDisplayName(), item.getFragmentClass().newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        navpane.listView.refresh();
        navpane.listView.requestLayout();

        navpane.logoutBtn.setOnAction(e -> {
            Registry.getInstance().setCurrentUser(null);
        });
    }


    public void toggleDrawer() {
        if (drawer.isDrawerOpened())
            drawer.closeDrawer();
        else
            drawer.openDrawer();
    }

    @RequiredArgsConstructor
    @Data
    private static class ViewItem {
        public final String displayName;
        public final Class<? extends Fragment> fragmentClass;
    }
}
