package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXDialog;
import javafx.application.Platform;
import javafx.beans.property.adapter.JavaBeanIntegerPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.data.IItemManager;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.data.inmem.items.InMemItem;
import me.yrf.ictse.jp.gui.dialogs.ItemEditDialog;
import me.yrf.ictse.jp.gui.dialogs.RequisitionDialog;
import me.yrf.ictse.jp.util.FXUtil;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ItemManagementFragment extends Fragment {
    @FXML
    private TableView<IItem> table;
    private IItemManager itemMan = Registry.getInstance().getDbManager().getItemManager();
    private ObservableList<IItem> items = FXCollections.observableList(new LinkedList<>());

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        fxmlLoader.setLocation(getClass().getResource("/gui/fragments/ItemManagement.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initializeTableView();
    }

    @SuppressWarnings("unchecked")
    private void initializeTableView() {
        val col1 = new TableColumn<IItem, Number>("Item ID");
        col1.setCellValueFactory(FXUtil::idBeanProperty);
        val col2 = new TableColumn<IItem, String>("Name");
        col2.setCellValueFactory(cd -> {
            try {
                return new JavaBeanStringPropertyBuilder()
                        .bean(cd.getValue()).name("name").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col3 = new TableColumn<IItem, String>("Description");
        col3.setCellValueFactory(cd -> {
            try {
                return new JavaBeanStringPropertyBuilder()
                        .bean(cd.getValue()).name("desc").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col4 = new TableColumn<IItem, Number>("Supplier ID");
        col4.setCellValueFactory(cd -> {
            try {
                return new JavaBeanIntegerPropertyBuilder()
                        .bean(cd.getValue()).name("supplierId").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });

        table.setItems(items);
        table.getColumns().setAll(col1, col2, col3, col4);
        table.setEditable(false);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        refreshView();
    }

    @FXML
    private void addItem() {
        try {
            val supCount = Registry.getInstance().getDbManager().getSupplierManager().count().get();
            if (supCount <= 0) {
                errDialog("No suppliers defined", "Items can not be defined without a supplier.");
                return;
            }

            val root = (StackPane) this.getScene().getRoot();
            val dialog = new ItemEditDialog(root, JFXDialog.DialogTransition.CENTER, null);
            dialog.setSaveHandler(() -> {
                if (!dialog.isEditing()) {
                    IItem item = new InMemItem();
                    if (dialog.getSupplier() == null)
                        return;
                    item.setSupplierId(dialog.getSupplier().getId());
                    item.setDesc(dialog.getDesc());
                    item.setName(dialog.getName());

                    CompletableFuture<IItem> retVal = itemMan.add(item);
                    retVal.whenComplete((v, e) -> {
                        if (v != null)
                            Platform.runLater(() -> items.add(v));
                    });
                }
                dialog.close();
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            errDialog("Datastore Error", "Unable to fetch suppliers!");
        }
    }

    @FXML
    private void editItem() {
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        try {
            val root = (StackPane) this.getScene().getRoot();
            val editedItem = table.getSelectionModel().getSelectedItem();
            val dialog = new ItemEditDialog(root, JFXDialog.DialogTransition.CENTER, editedItem);
            dialog.setSaveHandler(() -> {
                if (editedItem != null) {
                    editedItem.setName(dialog.getName());
                    editedItem.setSupplierId(dialog.getSupplier().getId());
                    editedItem.setDesc(dialog.getDesc());
                    itemMan.update(editedItem)
                            .whenComplete((v2, e2) -> {
                                if (v2 != null)
                                    items.set(items.indexOf(editedItem), v2);
                            });

                }
                dialog.close();
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void requisitionItems() {
        List<IItem> reqItems = new ArrayList<>(table.getSelectionModel().getSelectedItems());
        if (reqItems.isEmpty()) {
            errDialog("Requsitioning nothing", "Cannot place requisition for nothing!");
            return;
        }
        //Display requisition dialog.. maybe change to requisition view too?
        StackPane root = (StackPane) this.getScene().getRoot();
        try {
            RequisitionDialog dialog = new RequisitionDialog(root, JFXDialog.DialogTransition.CENTER, reqItems);
            dialog.show();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @FXML
    private void removeItem() {
        table.getSelectionModel()
                .getSelectedItems()
                .stream()
                .forEach(u -> itemMan.remove(u.getId())
                    .whenComplete((v, e) -> this.items.remove(u)));
    }

    private void refreshView() {
        itemMan.count()
                .thenCompose(c -> itemMan.getMany(0, c))
                .whenComplete((v, e) -> {
                    if (e == null) {
                        Platform.runLater(() -> this.items.setAll(v));
                    } else e.printStackTrace();
                });

    }

    private void errDialog(String title, String message) {
        MaterialButton button = new MaterialButton();
        button.setText("OK");
        val dialog = EntryPoint.errorDialog(title,
                new Exception(message),
                (Pane) this.getScene().getRoot(),
                button);
        button.setOnAction((a) -> {
            assert dialog != null;
            dialog.close();
        });
    }
}
