package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXDialog;
import javafx.application.Platform;
import javafx.beans.property.adapter.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import lombok.SneakyThrows;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.EnumPermission;
import me.yrf.ictse.jp.api.data.IOrderManager;
import me.yrf.ictse.jp.api.data.IRequisitionManager;
import me.yrf.ictse.jp.api.data.entities.IRequisition;
import me.yrf.ictse.jp.gui.dialogs.MessageDialog;
import me.yrf.ictse.jp.gui.dialogs.PromptDialog;
import me.yrf.ictse.jp.gui.dialogs.RequisitionDialog;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;

public class RequisitionsFragment extends Fragment {

    private final IRequisitionManager reqMan = Registry.getInstance().getDbManager().getRequisitionManager();
    private final IOrderManager ordMan = Registry.getInstance().getDbManager().getOrderManager();
    @FXML
    private TableView<IRequisition> table;
    @FXML
    private MaterialButton approveBtn;
    private ObservableList<IRequisition> reqs = FXCollections.observableList(new LinkedList<>());

    @Override
    @SuppressWarnings("unchecked")
    public void onCreateView(FXMLLoader fxmlLoader) {
        try {
            fxmlLoader.setLocation(getClass().getResource("/gui/fragments/RequisitionsFragment.fxml"));
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Hide requisition approval button if user is not allowed to place orders.
        if (!Registry.getInstance().getCurrentUser().get()
                .getAccessLevel().hasPermission(EnumPermission.ORDER)) {
            approveBtn.setVisible(false);
            approveBtn.setManaged(false);
        }

        table.setItems(reqs);
        reqMan.count()
                .thenCompose(c -> reqMan.getMany(0, c))
                .whenComplete((v, o0o) -> {
                    if (o0o == null) {
                        reqs.clear();
                        reqs.addAll(v);
                    }
                });

        val col1 = new TableColumn<IRequisition, Number>("Requisition ID");
        val col2 = new TableColumn<IRequisition, LocalDate>("Due By");
        val col3 = new TableColumn<IRequisition, String>("Created By");
        col1.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanIntegerPropertyBuilder()
                        .bean(cd.getValue()).name("id").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col2.setCellValueFactory(cd -> {
            try {
                return new JavaBeanObjectPropertyBuilder<LocalDate>()
                        .bean(cd.getValue()).name("dueDate").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col3.setCellValueFactory(cd -> {
            try {
                return new ReadOnlyJavaBeanStringPropertyBuilder()
                        .bean(cd.getValue().getCreator()).name("username").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        table.getColumns().setAll(col1, col2, col3);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.setRowFactory(tv -> {
            TableRow<IRequisition> row = new TableRow<>();
            row.setOnMouseClicked(e -> {
                if (row.isEmpty())
                    return;
                if (e.getClickCount() == 2) {
                    try {
                        new RequisitionDialog((Pane) this.getScene().getRoot(),
                                JFXDialog.DialogTransition.CENTER,
                                row.getItem(), true).show();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            return row;
        });
    }

    @FXML
    private void approveReq() {
        if (table.getSelectionModel().getSelectedItems().size() <= 0)
            return;
        try {
            val root = (Pane) this.getScene().getRoot();
            val dialog = new PromptDialog(root, JFXDialog.DialogTransition.CENTER);

            dialog.setOnYes(() -> {
                Platform.runLater(() ->
                        table.getSelectionModel().getSelectedItems().stream()
                                .forEach(req -> ordMan.approveReq(req)
                                        .thenAccept(ord -> this.reqs.remove(req)))
                );
                dialog.close();
            });
            dialog.setOnNo(dialog::close);

            dialog.setTitle("Confirm Requisition Approval");
            dialog.setText(String.format("Are you sure you want to approve these %s requisitions?",
                    table.getSelectionModel().getSelectedItems().size()));

            dialog.setOverlayCloseable(true);
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void modifyReq() {
        val editedItem = table.getSelectionModel().getSelectedItem();
        if (editedItem == null)
            return;
        try {
            val root = (Pane) this.getScene().getRoot();
            val dialog = new RequisitionDialog(root, JFXDialog.DialogTransition.CENTER, editedItem, false);
            dialog.setSaveCallback(req -> this.reqs.set(reqs.indexOf(editedItem), req));
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void delReq() {
        if (table.getSelectionModel().getSelectedItems().size() <= 0)
            return;
        try {
            val root = (Pane) this.getScene().getRoot();
            val dialog = new MessageDialog(root, JFXDialog.DialogTransition.CENTER);
            val btnYes = new MaterialButton();
            val btnNo = new MaterialButton();

            btnYes.setText("Yes");
            btnYes.setOnAction(e -> Platform.runLater(() -> {
                table.getSelectionModel().getSelectedItems().stream()
                        .forEach(i -> reqMan.remove(i.getId()).thenAccept(i2 -> reqs.remove(i)));
                dialog.close();
            }));
            btnNo.setText("No");
            btnNo.setOnAction(e -> dialog.close());
            btnNo.setFlated(true);

            dialog.setTitle("Confirm Requisition Removal");
            dialog.setText(String.format("Are you sure you want to remove these %s requisitions?",
                    table.getSelectionModel().getSelectedItems().size()));

            dialog.getButtonBox().getChildren().addAll(btnNo, btnYes);
            dialog.setOverlayCloseable(true);
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
