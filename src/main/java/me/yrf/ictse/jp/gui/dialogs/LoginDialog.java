package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.val;
import me.yrf.ictse.jp.api.Procedure;
import me.yrf.ictse.jp.api.gui.validation.ValidationResult;

import java.io.IOException;
import java.util.function.BiConsumer;
import java.util.function.Function;

@NoArgsConstructor
public class LoginDialog extends JFXDialogController {

    @FXML
    TextField username;
    @FXML
    PasswordField password;
    @FXML
    private Label usernameError;
    @FXML
    private Label passwordError;
    @Setter
    private Function<String, ValidationResult> usernameValidator;
    @Setter
    private Function<String, ValidationResult> passwordValidator;
    @Setter
    private BiConsumer<String, String> loginHandler;
    @Setter
    private Procedure cancelHandler;

    public LoginDialog(Pane container, JFXDialog.DialogTransition transition) throws IOException {
        val loader = new FXMLLoader(LoginDialog.class.getResource("/gui/dialogs/LoginDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();
        val dialog = getDialog();
        dialog.setDialogContainer(container);
        dialog.setContent(content);
        dialog.setTransitionType(transition);
        dialog.show();
        dialog.close();
    }

    @FXML
    private void initialize() {
        username.textProperty().addListener((o, ov, nv) -> {
            if (usernameValidator != null) {
                ValidationResult result = usernameValidator.apply(nv);
                if (result.isValid() && usernameError.isVisible())
                    usernameError.setVisible(false);
                else if (!result.isValid()) {
                    showUsernameError(result.getMessage());
                }
            } else
                usernameError.setVisible(false);
        });
        password.textProperty().addListener((o, ov, nv) -> {
            if (passwordValidator != null) {
                ValidationResult result = passwordValidator.apply(nv);
                if (result.isValid() && passwordError.isVisible())
                    passwordError.setVisible(false);
                else if (!result.isValid()) {
                    showPasswordError(result.getMessage());
                }
            } else
                passwordError.setVisible(false);
        });
    }

    public void showUsernameError(String error) {
        usernameError.setText(error);
        usernameError.setVisible(true);
    }

    public void showPasswordError(String error) {
        passwordError.setText(error);
        passwordError.setVisible(true);
    }

    public String getUsername() {
        return username.getText();
    }

    public String getPassword() {
        return password.getText();
    }

    public void onLogin() {
        if (loginHandler != null)
            loginHandler.accept(getUsername(), getPassword());
    }

    public void onCancel() {
        if (cancelHandler != null)
            cancelHandler.invoke();
    }

}
