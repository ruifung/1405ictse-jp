package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.VBox;
import me.yrf.ictse.jp.api.INavCallback;
import me.yrf.ictse.jp.util.FXUtil;
import org.kairos.components.MaterialButton;
import org.kairos.core.Fragment;

import java.io.IOException;

public class NavPaneFragment extends Fragment {

    @FXML
    public VBox vbox;
    @FXML
    public JFXListView<String> listView;
    @FXML
    public MaterialButton logoutBtn;

    protected ListCell<String> selected;
    private INavCallback<String> clickHandler;

    public NavPaneFragment(INavCallback<String> clickHandler) {
        this.clickHandler = clickHandler;
    }

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        fxmlLoader.setLocation(getClass().getResource("/gui/fragments/NavPane.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FXUtil.anchorMax(vbox);

        listView.setCellFactory(lv -> {
            JFXListCell<String> cell = new JFXListCell<>();
            if (this.selected == null) {
                this.selected = cell;
                cell.getStyleClass().add("selected");
            }
            cell.setOnMouseClicked(e -> {
                if (this.selected != null)
                    this.selected.getStyleClass().remove("selected");
                this.selected = cell;
                cell.getStyleClass().add("selected");

                if (cell.getItem() != null) {
                    if (clickHandler != null)
                        clickHandler.onItemClick(cell.getItem());
                }
            });
            return cell;
        });
    }
}
