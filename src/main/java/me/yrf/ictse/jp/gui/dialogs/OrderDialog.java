package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.adapter.JavaBeanIntegerPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.converter.NumberStringConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.api.data.entities.IOrder;
import me.yrf.ictse.jp.api.data.entities.IRequisition;
import me.yrf.ictse.jp.data.inmem.requisitions.InMemRequisition;
import org.kairos.components.MaterialButton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class OrderDialog extends JFXDialogController {

    private final int itemId;
    @FXML
    private TextField orderedOn, approver, dueDate;
    @FXML
    private Label title;
    @FXML
    private TextField creator;
    @FXML
    private TableView<Item> items;

    @FXML
    private MaterialButton closeBtn, addBtn, minusBtn;

    private IOrder order;

    @SuppressWarnings("unchecked")
    private OrderDialog(Pane container, JFXDialog.DialogTransition transition, int id) throws IOException {
        val loader = new FXMLLoader(this.getClass().getResource("/gui/dialogs/OrderDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();

        this.addBtn.setVisible(false);
        this.minusBtn.setVisible(false);

        this.getDialog().setDialogContainer(container);
        this.getDialog().setContent(content);
        this.setOverlayCloseable(false);
        this.getDialog().setTransitionType(transition);
        this.itemId = id;

        items.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        items.setEditable(true);
        val col1 = new TableColumn<Item, String>("Item");
        col1.setEditable(false);
        col1.setCellValueFactory(cd -> new SimpleStringProperty(cd.getValue().getItem().getName()));
        val col2 = new TableColumn<Item, Number>("Amount");
        col2.setEditable(true);
        col2.setCellValueFactory(cd -> {
            try {
                return new JavaBeanIntegerPropertyBuilder()
                        .bean(cd.getValue()).name("amount").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        col2.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
        items.getColumns().setAll(col1, col2);
    }

    public OrderDialog(Pane container, JFXDialog.DialogTransition transition,
                       @NonNull IOrder order) throws IOException {
        this(container, transition, order.getId());
        creator.setText(order.getCreator().getUsername());
        this.order = order;

        title.setText(String.format("View Requisition: #%s", order.getId()));
        dueDate.setEditable(false);
        items.setEditable(false);
        closeBtn.setFlated(false);
        closeBtn.setText("Close");
        this.approver.setText(order.getApprover().getUsername());
        this.orderedOn.setText(order.getOrderedOn().toString());
        this.dueDate.setText(order.getDueDate().toString());
        this.items.setItems(order.getItems().entrySet().stream()
                .map(e -> new Item(e.getKey(), e.getValue()))
                .collect(Collectors.collectingAndThen(Collectors.toList(), FXCollections::observableList)));
    }

    @FXML
    @Override
    public void close() {
        super.close();
    }

    @Data
    @AllArgsConstructor
    public static class Item {
        private IItem item;
        private int amount;
    }
}
