package me.yrf.ictse.jp.gui.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import me.yrf.ictse.jp.api.data.entities.ISupplier;

public class TreeTableSupplier extends RecursiveTreeObject<TreeTableSupplier> {

    private ReadOnlyIntegerProperty id;
    private StringProperty name;
    private StringProperty email;
    private StringProperty phone;
    private StringProperty address;

    public TreeTableSupplier(ISupplier supplier) {
        try {
            id = new ReadOnlyJavaBeanIntegerPropertyBuilder()
                    .bean(supplier).name("id").build();
            name = new JavaBeanStringPropertyBuilder()
                    .bean(supplier).name("name").build();
            email = new JavaBeanStringPropertyBuilder()
                    .bean(supplier).name("email").build();
            phone = new JavaBeanStringPropertyBuilder()
                    .bean(supplier).name("phone").build();
            address = new JavaBeanStringPropertyBuilder()
                    .bean(supplier).name("address").build();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id.get();
    }

    public ReadOnlyIntegerProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public StringProperty addressProperty() {
        return address;
    }
}
