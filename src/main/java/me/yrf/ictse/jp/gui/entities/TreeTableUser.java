package me.yrf.ictse.jp.gui.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanStringPropertyBuilder;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.api.auth.IUser;

public class TreeTableUser extends RecursiveTreeObject<TreeTableUser> {
    private ObservableIntegerValue id;
    private ObservableValue<String> username;
    private ObservableValue<String> fullName;
    private ObservableValue<EnumUserAccessLevel> userAccessLevel;

    @SuppressWarnings("unchecked")
    public TreeTableUser(IUser user) {
        try {
            id = new ReadOnlyJavaBeanIntegerPropertyBuilder()
                    .bean(user).name("id").build();
            username = new ReadOnlyJavaBeanStringPropertyBuilder()
                    .bean(user).name("username").build();
            fullName = new JavaBeanStringPropertyBuilder()
                    .bean(user).name("fullName").build();
            userAccessLevel = new JavaBeanObjectPropertyBuilder<EnumUserAccessLevel>()
                    .bean(user).name("accessLevel").build();

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public Number getId() {
        return id.get();
    }

    public ObservableIntegerValue idProperty() {
        return id;
    }

    public String getUsername() {
        return username.getValue();
    }

    public ObservableValue<String> usernameProperty() {
        return username;
    }

    public String getFullName() {
        return fullName.getValue();
    }

    public ObservableValue<String> fullNameProperty() {
        return fullName;
    }

    public EnumUserAccessLevel getUserAccessLevel() {
        return userAccessLevel.getValue();
    }

    public ObservableValue<EnumUserAccessLevel> userAccessLevelProperty() {
        return userAccessLevel;
    }
}
