/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package me.yrf.ictse.jp.gui.controls;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.events.JFXDialogEvent;
import com.jfoenix.converters.DialogTransitionConverter;
import com.jfoenix.effects.JFXDepthManager;
import com.jfoenix.transitions.CachedTransition;
import javafx.animation.*;
import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.css.*;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Shadi Shaheen
 *         note that for JFXDialog to work properly the root node should
 *         be of type {@link StackPane}
 */
@DefaultProperty(value = "content")
public class JFXDialogEx extends StackPane {

    /***************************************************************************
     * *
     * Stylesheet Handling                                                     *
     * *
     **************************************************************************/

    private static final String DEFAULT_STYLE_CLASS = "jfx-dialog";
    private StackPane contentHolder;
    private StackPane overlayPane;
    private double offsetX = 0;
    private double offsetY = 0;
    private Pane dialogContainer;
    private Region content;
    private Transition animation;
    private BooleanProperty overlayClose = new SimpleBooleanProperty(true);
    private StyleableObjectProperty<JFXDialog.DialogTransition> transitionType = new SimpleStyleableObjectProperty<>(StyleableProperties.DIALOG_TRANSITION, JFXDialogEx.this, "dialogTransition", JFXDialog.DialogTransition.CENTER);
    // inherit the styleable properties from parent
    private List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
    /***************************************************************************
     * *
     * Custom Events                                                           *
     * *
     **************************************************************************/

    private ObjectProperty<EventHandler<? super JFXDialogEvent>> onDialogClosedProperty = new SimpleObjectProperty<>((closed) -> {
    });
    EventHandler<? super MouseEvent> closeHandler = (e) -> close();
    private ObjectProperty<EventHandler<? super JFXDialogEvent>> onDialogOpenedProperty = new SimpleObjectProperty<>((opened) -> {
    });

    public JFXDialogEx() {
        this(null, null, JFXDialog.DialogTransition.CENTER);
    }

    /**
     * creates JFX Dialog control
     *
     * @param dialogContainer
     * @param content
     * @param transitionType
     */

    public JFXDialogEx(Pane dialogContainer, Region content, JFXDialog.DialogTransition transitionType) {
        initialize();
        setContent(content);
        setDialogContainer(dialogContainer);
        this.transitionType.set(transitionType);
        // init change listeners
        initChangeListeners();
    }

    /**
     * creates JFX Dialog control
     *
     * @param dialogContainer
     * @param content
     * @param transitionType
     * @param overlayClose
     */
    public JFXDialogEx(Pane dialogContainer, Region content, JFXDialog.DialogTransition transitionType, boolean overlayClose) {
        initialize();
        setOverlayClose(overlayClose);
        setContent(content);
        setDialogContainer(dialogContainer);
        this.transitionType.set(transitionType);
        // init change listeners
        initChangeListeners();
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return StyleableProperties.CHILD_STYLEABLES;
    }

    private void initChangeListeners() {
        overlayCloseProperty().addListener((o, oldVal, newVal) -> {
            if (overlayPane != null) {
                if (newVal) overlayPane.addEventHandler(MouseEvent.MOUSE_PRESSED, closeHandler);
                else overlayPane.removeEventHandler(MouseEvent.MOUSE_PRESSED, closeHandler);
            }
        });
    }

    private void initialize() {
        this.setVisible(false);
        this.getStyleClass().add(DEFAULT_STYLE_CLASS);

        contentHolder = new StackPane();
        contentHolder.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(2), null)));
        JFXDepthManager.setDepth(contentHolder, 4);
        contentHolder.setPickOnBounds(false);
        // ensure stackpane is never resized beyond it's preferred size
        contentHolder.setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        overlayPane = new StackPane();
        overlayPane.getChildren().add(contentHolder);
        overlayPane.getStyleClass().add("jfx-dialog-overlay-pane");
        StackPane.setAlignment(contentHolder, Pos.CENTER);
        overlayPane.setVisible(false);
        overlayPane.setBackground(new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.1), null, null)));
        // close the dialog if clicked on the overlay pane
        if (overlayClose.get()) overlayPane.addEventHandler(MouseEvent.MOUSE_PRESSED, closeHandler);
        // prevent propagating the events to overlay pane
        contentHolder.addEventHandler(MouseEvent.ANY, Event::consume);
    }

    /***************************************************************************
     * *
     * Setters / Getters                                                       *
     * *
     **************************************************************************/

    public Pane getDialogContainer() {
        return dialogContainer;
    }

    public void setDialogContainer(Pane dialogContainer) {
        if (dialogContainer != null) {
            this.dialogContainer = dialogContainer;
            if (this.getChildren().indexOf(overlayPane) == -1) this.getChildren().setAll(overlayPane);
            this.visibleProperty().unbind();
            this.visibleProperty().bind(overlayPane.visibleProperty());
            if (this.dialogContainer.getChildren().indexOf(this) == -1 || this.dialogContainer.getChildren().indexOf(this) != this.dialogContainer.getChildren().size() - 1) {
                this.dialogContainer.getChildren().remove(this);
                this.dialogContainer.getChildren().add(this);
            }
            // FIXME: need to be improved to consider only the parent boundary
            offsetX = (this.getParent().getBoundsInLocal().getWidth());
            offsetY = (this.getParent().getBoundsInLocal().getHeight());
            animation = getShowAnimation(transitionType.get());
        }
    }

    public Region getContent() {
        return content;
    }

    public void setContent(Region content) {
        if (content != null) {
            this.content = content;
            contentHolder.getChildren().add(content);
        }
    }

    public final BooleanProperty overlayCloseProperty() {
        return this.overlayClose;
    }

    public final boolean isOverlayClose() {
        return this.overlayCloseProperty().get();
    }

    public final void setOverlayClose(final boolean overlayClose) {
        this.overlayCloseProperty().set(overlayClose);
    }

    /***************************************************************************
     * *
     * Public API                                                              *
     * *
     **************************************************************************/

    public void show(Pane dialogContainer) {
        this.setDialogContainer(dialogContainer);
        animation.play();
    }

    public void show() {
        this.setDialogContainer(dialogContainer);
        animation = getShowAnimation(transitionType.get());
        animation.play();
    }

    public void close() {
        animation.setRate(-1);
        animation.play();
        animation.setOnFinished((e) -> resetProperties());
        onDialogClosedProperty.get().handle(new JFXDialogEvent(JFXDialogEvent.CLOSED));
    }

    /***************************************************************************
     * *
     * Transitions                                                             *
     * *
     **************************************************************************/

    private Transition getShowAnimation(JFXDialog.DialogTransition transitionType) {
        Transition animation = null;
        if (contentHolder != null) {
            switch (transitionType) {
                case LEFT:
                    contentHolder.setTranslateX(-offsetX);
                    animation = new LeftTransition();
                    break;
                case RIGHT:
                    contentHolder.setTranslateX(offsetX);
                    animation = new RightTransition();
                    break;
                case TOP:
                    contentHolder.setTranslateY(-offsetY);
                    animation = new TopTransition();
                    break;
                case BOTTOM:
                    contentHolder.setTranslateY(offsetY);
                    animation = new BottomTransition();
                    break;
                default:
                    contentHolder.setScaleX(0);
                    contentHolder.setScaleY(0);
                    animation = new CenterTransition();
                    break;
            }
        }
        if (animation != null)
            animation.setOnFinished((finish) -> onDialogOpenedProperty.get().handle(new JFXDialogEvent(JFXDialogEvent.OPENED)));
        return animation;
    }

    private void resetProperties() {
        overlayPane.setVisible(false);
        contentHolder.setTranslateX(0);
        contentHolder.setTranslateY(0);
        contentHolder.setScaleX(1);
        contentHolder.setScaleY(1);
    }

    public JFXDialog.DialogTransition getTransitionType() {
        return transitionType == null ? JFXDialog.DialogTransition.CENTER : transitionType.get();
    }

    public void setTransitionType(JFXDialog.DialogTransition transition) {
        this.transitionType.set(transition);
    }

    public StyleableObjectProperty<JFXDialog.DialogTransition> transitionTypeProperty() {
        return this.transitionType;
    }

    @Override
    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        if (STYLEABLES == null) {
            final List<CssMetaData<? extends Styleable, ?>> styleables =
                    new ArrayList<>(Parent.getClassCssMetaData());
            styleables.addAll(getClassCssMetaData());
            styleables.addAll(StackPane.getClassCssMetaData());
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
        return STYLEABLES;
    }

    public EventHandler<? super JFXDialogEvent> getOnDialogClosed() {
        return onDialogClosedProperty.get();
    }

    public void setOnDialogClosed(EventHandler<? super JFXDialogEvent> handler) {
        onDialogClosedProperty.set(handler);
    }

    public EventHandler<? super JFXDialogEvent> getOnDialogOpened() {
        return onDialogOpenedProperty.get();
    }

    public void setOnDialogOpened(EventHandler<? super JFXDialogEvent> handler) {
        onDialogOpenedProperty.set(handler);
    }

    private static class StyleableProperties {
        private static final CssMetaData<JFXDialogEx, JFXDialog.DialogTransition> DIALOG_TRANSITION =
                new CssMetaData<JFXDialogEx, JFXDialog.DialogTransition>("-fx-dialog-transition",
                        DialogTransitionConverter.getInstance(), JFXDialog.DialogTransition.CENTER) {
                    @Override
                    public boolean isSettable(JFXDialogEx control) {
                        return control.transitionType == null || !control.transitionType.isBound();
                    }

                    @Override
                    public StyleableProperty<JFXDialog.DialogTransition> getStyleableProperty(JFXDialogEx control) {
                        return control.transitionTypeProperty();
                    }
                };

        private static final List<CssMetaData<? extends Styleable, ?>> CHILD_STYLEABLES;

        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables =
                    new ArrayList<>(Parent.getClassCssMetaData());
            Collections.addAll(styleables,
                    DIALOG_TRANSITION
            );
            CHILD_STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    private class LeftTransition extends CachedTransition {
        public LeftTransition() {
            super(contentHolder, new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(contentHolder.translateXProperty(), -offsetX, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.visibleProperty(), false, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(10),
                            new KeyValue(overlayPane.visibleProperty(), true, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(1000),
                            new KeyValue(contentHolder.translateXProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 1, Interpolator.EASE_BOTH)
                    ))
            );
            // reduce the number to increase the shifting , increase number to reduce shifting
            setCycleDuration(Duration.seconds(0.4));
            setDelay(Duration.seconds(0));
        }
    }

    private class RightTransition extends CachedTransition {
        public RightTransition() {
            super(contentHolder, new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(contentHolder.translateXProperty(), offsetX, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.visibleProperty(), false, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(10),
                            new KeyValue(overlayPane.visibleProperty(), true, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(1000),
                            new KeyValue(contentHolder.translateXProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 1, Interpolator.EASE_BOTH)))
            );
            // reduce the number to increase the shifting , increase number to reduce shifting
            setCycleDuration(Duration.seconds(0.4));
            setDelay(Duration.seconds(0));
        }
    }

    private class TopTransition extends CachedTransition {
        public TopTransition() {
            super(contentHolder, new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(contentHolder.translateYProperty(), -offsetY, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.visibleProperty(), false, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(10),
                            new KeyValue(overlayPane.visibleProperty(), true, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(1000),
                            new KeyValue(contentHolder.translateYProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 1, Interpolator.EASE_BOTH)))
            );
            // reduce the number to increase the shifting , increase number to reduce shifting
            setCycleDuration(Duration.seconds(0.4));
            setDelay(Duration.seconds(0));
        }
    }

    private class BottomTransition extends CachedTransition {
        public BottomTransition() {
            super(contentHolder, new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(contentHolder.translateYProperty(), offsetY, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.visibleProperty(), false, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(10),
                            new KeyValue(overlayPane.visibleProperty(), true, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(1000),
                            new KeyValue(contentHolder.translateYProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 1, Interpolator.EASE_BOTH)))
            );
            // reduce the number to increase the shifting , increase number to reduce shifting
            setCycleDuration(Duration.seconds(0.4));
            setDelay(Duration.seconds(0));
        }
    }

    private class CenterTransition extends CachedTransition {
        public CenterTransition() {
            super(contentHolder, new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(contentHolder.scaleXProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(contentHolder.scaleYProperty(), 0, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.visibleProperty(), false, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(10),
                            new KeyValue(overlayPane.visibleProperty(), true, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(1000),
                            new KeyValue(contentHolder.scaleXProperty(), 1, Interpolator.EASE_BOTH),
                            new KeyValue(contentHolder.scaleYProperty(), 1, Interpolator.EASE_BOTH),
                            new KeyValue(overlayPane.opacityProperty(), 1, Interpolator.EASE_BOTH)
                    ))
            );
            // reduce the number to increase the shifting , increase number to reduce shifting
            setCycleDuration(Duration.seconds(0.4));
            setDelay(Duration.seconds(0));
        }
    }


}

