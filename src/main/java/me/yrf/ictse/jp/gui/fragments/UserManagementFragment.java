package me.yrf.ictse.jp.gui.fragments;

import com.jfoenix.controls.JFXDialog;
import javafx.application.Platform;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanStringPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.EnumPermission;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.api.auth.IUser;
import me.yrf.ictse.jp.api.auth.IUserManager;
import me.yrf.ictse.jp.data.inmem.users.InMemUser;
import me.yrf.ictse.jp.exceptions.InvalidOperationException;
import me.yrf.ictse.jp.gui.dialogs.UserEditDialog;
import org.kairos.core.Fragment;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.CompletableFuture;

public class UserManagementFragment extends Fragment {
    private final IUserManager userMan = Registry.getInstance().getDbManager().getUserManager();
    private final Byte userLock = 0;
    @FXML
    private VBox vbox;
    @FXML
    private TableView<IUser> table;
    private ObservableList<IUser> users = FXCollections.observableList(new LinkedList<>());

    @Override
    public void onCreateView(FXMLLoader fxmlLoader) {
        fxmlLoader.setLocation(getClass().getResource("/gui/fragments/UserManagement.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        initializeTableView();
    }

    @SuppressWarnings("unchecked")
    private void initializeTableView() {
        val col1 = new TableColumn<IUser, String>("Username");
        col1.setCellValueFactory(cd -> {
            try {
                return ReadOnlyJavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("username").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col2 = new TableColumn<IUser, String>("Full Name");
        col2.setCellValueFactory(cd -> {
            try {
                return JavaBeanStringPropertyBuilder.create()
                        .bean(cd.getValue()).name("fullName").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });
        val col3 = new TableColumn<IUser, EnumUserAccessLevel>("Access Level");
        col3.setCellValueFactory(cd -> {
            try {
                return JavaBeanObjectPropertyBuilder.<EnumUserAccessLevel>create()
                        .bean(cd.getValue()).name("accessLevel").build();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return null;
            }
        });

        table.getColumns().setAll(col1, col2, col3);
        table.setEditable(false);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setItems(users);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        refreshView();
    }

    @FXML
    private void addUser() {
        try {
            val root = (StackPane) this.getScene().getRoot();
            val dialog = new UserEditDialog(root, JFXDialog.DialogTransition.CENTER, null);
            dialog.setSaveHandler(() -> {
                IUser user = new InMemUser(dialog.getUsername());
                user.setFullName(dialog.getFullName());
                user.setAccessLevel(dialog.getAccessLevel());
                CompletableFuture<IUser> future = userMan.add(user, dialog.getPassword());
                future.whenComplete((v, e) -> {
                    if (e != null) {
                        e.printStackTrace();
                    } else {
                        synchronized (userLock) {
                            this.users.add(v);
                        }
                        dialog.close();
                    }
                });
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void editUser() {
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        try {
            val root = (StackPane) this.getScene().getRoot();
            val editedUser = table.getSelectionModel().getSelectedItem();
            val dialog = new UserEditDialog(root, JFXDialog.DialogTransition.CENTER, editedUser);
            dialog.setSaveHandler(() -> {
                editedUser.setFullName(dialog.getFullName());
                //Just to prevent admins from locking themselves out (accidentally).
                if ( !editedUser.getUsername().equals(Registry.getInstance().getCurrentUser().get().getUsername()) ||
                        dialog.getAccessLevel().hasAllPermissions(editedUser.getAccessLevel()
                        .getEnumPermissions()
                        .toArray(new EnumPermission[editedUser.getAccessLevel().getEnumPermissions().size()])
                ))
                    editedUser.setAccessLevel(dialog.getAccessLevel());
                if (dialog.getPassword() != null && !dialog.getPassword().isEmpty())
                    editedUser.setPassword(dialog.getPassword());
                try {
                    CompletableFuture<IUser> updated = userMan.update(editedUser);
                    updated.whenComplete((v2, e2) -> Platform.runLater(() -> {
                        if (e2 != null)
                            return;
                        int index = users.indexOf(editedUser);
                        users.set(index, v2);
                    }));

                } catch (InvalidOperationException o0o) {
                    o0o.printStackTrace();
                }


                dialog.close();
            });
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void removeUser() {
        val userMan = Registry.getInstance().getDbManager().getUserManager();
        table.getSelectionModel()
                .getSelectedItems()
                .stream()
                .filter(i -> !i.getUsername()
                        .equals(Registry.getInstance().getCurrentUser().get().getUsername()))
                .forEach(u -> {
                    CompletableFuture<Void> future = userMan.remove(u.getUsername());
                    future.whenComplete((v, e) -> {
                        if (e == null) {
                            synchronized (userLock) {
                                this.users.remove(u);
                            }
                        }
                    });
                });
    }

    private void refreshView() {
        userMan.count().thenCompose(c -> userMan.getMany(0, c))
                .whenComplete((v, e) -> {
                    if (e != null) {
                        return;
                    }
                    synchronized (userLock) {
                        users.clear();
                        users.addAll(v);
                    }
                });
    }
}
