package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.api.Procedure;
import me.yrf.ictse.jp.api.data.entities.ISupplier;

import java.io.IOException;

public class SupplierEditDialog extends JFXDialogController {

    private final int itemId;
    @FXML
    Label title;
    @FXML
    TextField name;
    @FXML
    TextField email;
    @FXML
    TextField phone;
    @FXML
    TextArea address;
    private Procedure saveHandler;
    private boolean isEditing;

    public SupplierEditDialog(Pane container, JFXDialog.DialogTransition transition, ISupplier item) throws IOException {
        val loader = new FXMLLoader(this.getClass().getResource("/gui/dialogs/SupplierEditDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();

        this.getDialog().setDialogContainer(container);
        this.getDialog().setContent(content);
        this.setOverlayCloseable(false);
        this.getDialog().setTransitionType(transition);

        if (item != null) {
            isEditing = true;
            itemId = item.getId();
            title.setText(String.format("Editing Supplier: #%s", item.getId()));
            name.setText(item.getName());
            email.setText(item.getEmail());
            phone.setText(item.getPhone());
            address.setText(item.getAddress());

        } else {
            isEditing = false;
            itemId = -1;
            title.setText("Add New Supplier");
        }
    }

    @FXML
    private void onSave(Event e) {
        if (this.saveHandler != null)
            this.saveHandler.invoke();
    }

    public Procedure getSaveHandler() {
        return saveHandler;
    }

    public void setSaveHandler(Procedure saveHandler) {
        this.saveHandler = saveHandler;
    }

    public boolean isEditing() {
        return isEditing;
    }

    public int getItemId() {
        return itemId;
    }

    public String getName() {
        return name.getText();
    }

    public String getEmail() {
        return email.getText();
    }

    public String getPhone() {
        return phone.getText();
    }

    public String getAddress() {
        return address.getText();
    }


    @FXML
    @Override
    public void close() {
        super.close();
    }
}
