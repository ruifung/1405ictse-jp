package me.yrf.ictse.jp.gui.entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.adapter.JavaBeanIntegerPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.beans.property.adapter.ReadOnlyJavaBeanIntegerPropertyBuilder;
import javafx.beans.value.ObservableValue;
import me.yrf.ictse.jp.api.data.entities.IItem;

public class TreeTableIItem extends RecursiveTreeObject<TreeTableIItem> {
    private ReadOnlyIntegerProperty id;
    private StringProperty name;
    private StringProperty desc;
    private IntegerProperty supplierId;

    public TreeTableIItem(IItem user) {
        try {
            id = new ReadOnlyJavaBeanIntegerPropertyBuilder()
                    .bean(user).name("id").build();
            name = new JavaBeanStringPropertyBuilder()
                    .bean(user).name("name").build();
            desc = new JavaBeanStringPropertyBuilder()
                    .bean(user).name("desc").build();
            supplierId = new JavaBeanIntegerPropertyBuilder()
                    .bean(user).name("supplierId").build();

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id.get();
    }

    public ReadOnlyIntegerProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public ObservableValue<String> nameProperty() {
        return name;
    }

    public String getDesc() {
        return desc.get();
    }

    public ObservableValue<String> descProperty() {
        return desc;
    }

    public int getSupplierId() {
        return supplierId.get();
    }

    public IntegerProperty supplierIdProperty() {
        return supplierId;
    }
}
