package me.yrf.ictse.jp.gui.controls;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.kairos.Toolbar;
import org.kairos.components.IconButton;
import org.kairos.core.ActionBar;

/**
 * Copied from Project Kairos for modification.
 */
public class ToolbarEx extends VBox implements ActionBar {
    private final Label title = new Label();
    private final IconButton home = new IconButton();
    @FXML
    private HBox topPlace = new HBox();
    @FXML
    private HBox topRightPlace = new HBox();
    private boolean homeAsUp = false;

    public ToolbarEx() {
        this.getStyleClass().add("toolbar");
        this.setAlignment(Pos.CENTER_LEFT);
        this.topPlace.setMinHeight(64.0D);
        this.topPlace.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(topRightPlace, Priority.ALWAYS);
        this.topRightPlace.setAlignment(Pos.CENTER_RIGHT);
        this.topPlace.getChildren().addAll(this.title, this.topRightPlace);
        this.topPlace.getStyleClass().add("top-place");
        this.title.getStyleClass().add("title");
        this.home.setIcon(Toolbar.class.getResourceAsStream("menu.svg"));
        this.home.setIconBackground(Toolbar.class.getResourceAsStream("arrow_back.svg"));
        this.getChildren().add(this.topPlace);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setDisplayShowHomeEnabled(boolean showHome) {
        if(showHome) {
            if (!this.topPlace.getChildren().contains(this.home)) {
                this.topPlace.getChildren().add(0, this.home);
            }
        }
        else {
            if (this.topPlace.getChildren().contains(this.home)) {
                this.topPlace.getChildren().remove(this.home);
            }
        }

    }

    public void showHomeAsUp() {
        setHomeVisible(true);
    }

    public void setHomeVisible(boolean flag) {
        this.home.setVisible(flag);
        this.home.setManaged(flag);
    }

    public boolean isDisplayHomeAsUpEnabled() {
        return this.homeAsUp;
    }

    public void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
        this.homeAsUp = showHomeAsUp;
        if (showHomeAsUp) {
            this.home.setIcon(Toolbar.class.getResourceAsStream("arrow_back.svg"));
            this.home.setVisible(false);
            this.home.setManaged(false);
        } else {
            this.home.setIcon(Toolbar.class.getResourceAsStream("menu.svg"));
            this.home.setVisible(true);
            this.home.setManaged(true);
        }
    }

    public void setOnHomeButtonAction(EventHandler<ActionEvent> handler) {
        this.home.setOnAction(handler);
    }

    public HBox getTopPlace() {
        return topPlace;
    }

    private void setTopPlace(HBox topPlace) {
        this.topPlace = topPlace;
    }

    public HBox getTopRightPlace() {
        return topRightPlace;
    }

    private void setTopRightPlace(HBox topRightPlace) {
        this.topRightPlace = topRightPlace;
    }
}