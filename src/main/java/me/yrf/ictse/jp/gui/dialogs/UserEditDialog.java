package me.yrf.ictse.jp.gui.dialogs;

import com.jfoenix.controls.JFXDialog;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import lombok.val;
import me.yrf.ictse.jp.api.Procedure;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.api.auth.IUser;

import java.io.IOException;

public class UserEditDialog extends JFXDialogController {

    @FXML
    Label title;
    @FXML
    private TextField username;
    @FXML
    private TextField fullName;
    @FXML
    private PasswordField password;
    @FXML
    private ComboBox<EnumUserAccessLevel> accessLevel;

    private int userId;
    private Procedure saveHandler;
    private boolean isEditing;

    public UserEditDialog(Pane container, JFXDialog.DialogTransition transition, IUser user) throws IOException {
        val loader = new FXMLLoader(this.getClass().getResource("/gui/dialogs/UserEditDialog.fxml"));
        loader.setController(this);
        Region content = loader.load();

        this.getDialog().setDialogContainer(container);
        this.getDialog().setContent(content);
        this.setOverlayCloseable(false);
        this.getDialog().setTransitionType(transition);

        accessLevel.setItems(FXCollections.observableArrayList(EnumUserAccessLevel.values()));

        if (user != null) {
            isEditing = true;
            userId = user.getId();
            username.setText(user.getUsername());
            username.setEditable(false);
            fullName.setText(user.getFullName());
            accessLevel.getSelectionModel().select(user.getAccessLevel());
            title.setText(String.format("Editing user: %s", user.getUsername()));
        } else {
            isEditing = false;
            username.setEditable(true);
            title.setText("Add New User");
        }
    }

    @FXML
    private void onSave(Event e) {
        if (this.saveHandler != null)
            this.saveHandler.invoke();
    }

    @Override
    @FXML
    public void close() {
        super.close();
    }

    public Procedure getSaveHandler() {
        return saveHandler;
    }

    public void setSaveHandler(Procedure saveHandler) {
        this.saveHandler = saveHandler;
    }

    public boolean isEditing() {
        return isEditing;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username.getText();
    }

    public String getFullName() {
        return fullName.getText();
    }

    public String getPassword() {
        return password.getText();
    }

    public EnumUserAccessLevel getAccessLevel() {
        return accessLevel.getSelectionModel().getSelectedItem();
    }
}
