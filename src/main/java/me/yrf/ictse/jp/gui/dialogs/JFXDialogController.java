package me.yrf.ictse.jp.gui.dialogs;

import lombok.Getter;
import lombok.val;
import me.yrf.ictse.jp.EntryPoint;
import me.yrf.ictse.jp.gui.controls.JFXDialogEx;
import org.kairos.components.MaterialButton;

public abstract class JFXDialogController {

    @Getter
    private JFXDialogEx dialog = new JFXDialogEx() {
        @Override
        public void close() {
            if (dialog.getDialogContainer() != null)
                dialog.getDialogContainer().getChildren().remove(this);
            super.close();
        }
    };

    public void close() {
        dialog.close();
    }

    public void setOverlayCloseable(boolean flag) {
        dialog.setOverlayClose(flag);
    }

    public void show() {
        this.dialog.show();
    }

    protected void errDialog(String title, String message) {
        MaterialButton button = new MaterialButton();
        button.setText("OK");
        val dialog = EntryPoint.errorDialog(title,
                new Exception(message),
                getDialog().getDialogContainer(),
                button);
        button.setOnAction((a) -> {
            assert dialog != null;
            dialog.close();
        });
    }
}
