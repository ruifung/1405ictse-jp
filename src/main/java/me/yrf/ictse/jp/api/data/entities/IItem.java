package me.yrf.ictse.jp.api.data.entities;

public interface IItem extends IIdEntity {

    String getName();

    void setName(String name);

    String getDesc();

    void setDesc(String desc);

    int getSupplierId();

    void setSupplierId(int supplierId);
}
