package me.yrf.ictse.jp.api.auth;

import me.yrf.ictse.jp.api.data.entities.IIdEntity;

public interface IUser extends IIdEntity {
    String getUsername();

    EnumUserAccessLevel getAccessLevel();
    void setAccessLevel(EnumUserAccessLevel level);

    String getFullName();
    void setFullName(String fullName);

    boolean checkPassword(String password);
    boolean setPassword(String password);
}