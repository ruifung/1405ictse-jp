package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.auth.IUserManager;

import java.io.Closeable;

public interface IDBManager extends Closeable {
    IUserManager getUserManager();
    IItemManager getItemManager();
    IOrderManager getOrderManager();
    IRequisitionManager getRequisitionManager();

    ISupplierManager getSupplierManager();

}
