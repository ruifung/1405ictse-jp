package me.yrf.ictse.jp.api;

@FunctionalInterface
public interface Procedure {
    void invoke();
}
