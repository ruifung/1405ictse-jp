package me.yrf.ictse.jp.api.gui.validation;

public abstract class ValidationResult {

    public static ValidationResult success() {
        return new Success();
    }

    public static ValidationResult failure(String reason) {
        return new Failure(reason);
    }

    private String message;
    public ValidationResult(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    abstract public boolean isValid();

    private static class Success extends ValidationResult {
        public Success() {
            super("");
        }

        @Override
        public boolean isValid() {
            return true;
        }
    }

    private static class Failure extends ValidationResult {
        public Failure(String reason) {
            super(reason);
        }

        @Override
        public boolean isValid() {
            return false;
        }
    }
}

