package me.yrf.ictse.jp.api.auth;

import me.yrf.ictse.jp.exceptions.InvalidOperationException;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IUserManager {

    CompletableFuture<IUser> get(String username);

    CompletableFuture<IUser> get(int id);

    CompletableFuture<Void> remove(String username);

    CompletableFuture<IUser> add(IUser user, String password);

    CompletableFuture<IUser> update(IUser user) throws InvalidOperationException;

    IAuthenticator getAuthenticator();

    CompletableFuture<Integer> count();

    CompletableFuture<List<IUser>> getMany(int startIndex, int amount);
}
