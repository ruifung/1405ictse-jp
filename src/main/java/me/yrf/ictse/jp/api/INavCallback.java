package me.yrf.ictse.jp.api;

@FunctionalInterface
public interface INavCallback<T> {
    void onItemClick(T item);
}
