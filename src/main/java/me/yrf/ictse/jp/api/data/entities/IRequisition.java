package me.yrf.ictse.jp.api.data.entities;

import me.yrf.ictse.jp.api.auth.IUser;

import java.time.LocalDate;
import java.util.Map;

public interface IRequisition extends IIdEntity {

    LocalDate getDueDate();

    void setDueDate(LocalDate date);

    IUser getCreator();

    void setCreator(IUser creator);

    Map<IItem, Integer> getItems();

    void setItems(Map<IItem, Integer> items);
}
