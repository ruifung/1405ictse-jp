package me.yrf.ictse.jp.api.auth;

import java.util.Optional;

public interface IAuthResult {
    boolean isAuthenticated();

    Optional<IUser> getUser();
}
