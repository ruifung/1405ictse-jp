package me.yrf.ictse.jp.api.auth;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum EnumUserAccessLevel {
    STAFF(EnumPermission.REQUISITION, EnumPermission.SUPPLIERS, EnumPermission.ITEMS),
    MANAGER(STAFF, EnumPermission.ORDER),
    ADMINISTRATOR(MANAGER, EnumPermission.ADMINISTRATE);

    private EnumSet<EnumPermission> enumPermissions;
    EnumUserAccessLevel(EnumPermission... permissions) {
        this(null, permissions);
    }
    EnumUserAccessLevel(EnumUserAccessLevel inherits, EnumPermission... permissions) {
        Stream<EnumPermission> thisPerms = Arrays.stream(permissions);
        if (inherits != null)
            thisPerms = Stream.concat(inherits.enumPermissions.stream(), thisPerms);
        enumPermissions = thisPerms.collect(Collectors.toCollection(() ->
                EnumSet.noneOf(EnumPermission.class)));
    }

    public EnumSet<EnumPermission> getEnumPermissions() {
        return enumPermissions;
    }

    public boolean hasPermission(EnumPermission perm) {
        return enumPermissions.contains(perm);
    }

    public boolean hasAllPermissions(EnumPermission... perms) {
        return Arrays.stream(perms).allMatch(enumPermissions::contains);
    }

    public boolean hasAnyPermission(EnumPermission... perms) {
        return Arrays.stream(perms).anyMatch(enumPermissions::contains);
    }
}
