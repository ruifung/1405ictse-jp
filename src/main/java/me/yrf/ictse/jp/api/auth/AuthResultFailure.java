package me.yrf.ictse.jp.api.auth;

import java.util.Optional;

public class AuthResultFailure implements IAuthResult{
    private String reason;
    public AuthResultFailure(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public Optional<IUser> getUser() {
        return Optional.empty();
    }
}
