package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.data.entities.IOrder;
import me.yrf.ictse.jp.api.data.entities.IRequisition;

import java.util.concurrent.CompletableFuture;

public interface IOrderManager extends IIdManager<IOrder> {
    CompletableFuture<IOrder> approveReq(IRequisition req);
}
