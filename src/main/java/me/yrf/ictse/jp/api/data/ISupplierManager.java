package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.data.entities.ISupplier;

public interface ISupplierManager extends IIdManager<ISupplier> {

}
