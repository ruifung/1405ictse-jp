package me.yrf.ictse.jp.api.data.entities;

public interface IIdEntity {
    int getId();
}
