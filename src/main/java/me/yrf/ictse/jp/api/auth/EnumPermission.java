package me.yrf.ictse.jp.api.auth;

public enum EnumPermission {
    REQUISITION,
    ORDER,
    SUPPLIERS,
    ITEMS,
    ADMINISTRATE
}
