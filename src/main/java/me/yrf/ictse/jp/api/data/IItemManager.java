package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.data.entities.IItem;

public interface IItemManager extends IIdManager<IItem> {

}
