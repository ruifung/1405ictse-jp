package me.yrf.ictse.jp.api.data.entities;

public interface ISupplier extends IIdEntity {

    String getName();

    void setName(String name);

    String getAddress();

    void setAddress(String address);

    String getEmail();

    void setEmail(String email);

    String getPhone();

    void setPhone(String phone);
}
