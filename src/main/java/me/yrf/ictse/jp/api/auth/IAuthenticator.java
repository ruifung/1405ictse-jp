package me.yrf.ictse.jp.api.auth;

@FunctionalInterface
public interface IAuthenticator {
    IAuthResult authenticate(String username, String password);
}
