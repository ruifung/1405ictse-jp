package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.data.entities.IIdEntity;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IIdManager<T extends IIdEntity> {
    CompletableFuture<T> get(int id);

    CompletableFuture<T> add(T item);

    CompletableFuture<T> update(T item);

    CompletableFuture<T> remove(int id);

    CompletableFuture<Integer> count();

    CompletableFuture<List<T>> getMany(int startIndex, int amount);
}
