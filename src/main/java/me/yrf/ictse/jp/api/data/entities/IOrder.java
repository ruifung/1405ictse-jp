package me.yrf.ictse.jp.api.data.entities;

import me.yrf.ictse.jp.api.auth.IUser;

import java.time.ZonedDateTime;

public interface IOrder extends IRequisition {
    IUser getApprover();

    void setApprover(IUser user);

    ZonedDateTime getOrderedOn();

    void setOrderedOn(ZonedDateTime orderedOn);
}
