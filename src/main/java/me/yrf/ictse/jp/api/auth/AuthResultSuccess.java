package me.yrf.ictse.jp.api.auth;

import java.util.Optional;

public class AuthResultSuccess implements IAuthResult {
    private IUser user;

    public AuthResultSuccess(IUser user) {
        this.user = user;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public Optional<IUser> getUser() {
        return Optional.ofNullable(user);
    }
}
