package me.yrf.ictse.jp.api.data;

import me.yrf.ictse.jp.api.data.entities.IRequisition;

public interface IRequisitionManager extends IIdManager<IRequisition> {

}
