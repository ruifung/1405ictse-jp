package me.yrf.ictse.jp;

import javafx.beans.property.SimpleObjectProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import me.yrf.ictse.jp.api.auth.IUser;
import me.yrf.ictse.jp.api.data.IDBManager;
import me.yrf.ictse.jp.exceptions.InvalidOperationException;
import me.yrf.ictse.jp.util.LazyVal;

import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Registry {
    private static LazyVal<Registry> instance = new LazyVal<>(Registry::new);
    @Getter
    private final SimpleObjectProperty<Optional<IUser>> currentUserProperty = new SimpleObjectProperty<>(Optional.empty());
    @Getter
    private IDBManager dbManager;

    public static Registry getInstance() {
        return instance.get();
    }

    public void setDbManager(IDBManager dbManager)
            throws InvalidOperationException {
        if (this.dbManager == null)
            this.dbManager = dbManager;
        else
            throw new InvalidOperationException();
    }

    public Optional<IUser> getCurrentUser() {
        return currentUserProperty.get();
    }

    public void setCurrentUser(IUser user) {
        currentUserProperty.set(Optional.ofNullable(user));
    }

}
