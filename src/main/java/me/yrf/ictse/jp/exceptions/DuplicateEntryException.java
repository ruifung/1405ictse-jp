package me.yrf.ictse.jp.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DuplicateEntryException extends Exception {
    public DuplicateEntryException(Throwable t) {
        super(t.getMessage(), t);
    }
}
