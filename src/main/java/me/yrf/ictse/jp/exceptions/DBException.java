package me.yrf.ictse.jp.exceptions;

public class DBException extends Exception {

    public DBException(Throwable t) {
        super(t);
    }
}
