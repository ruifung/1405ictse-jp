package me.yrf.ictse.jp.data.inmem.suppliers;

import me.yrf.ictse.jp.api.data.entities.ISupplier;

public class InMemSupplier implements ISupplier {
    private final int id;
    private String name;
    private String address;
    private String email;
    private String phone;

    InMemSupplier(int id) {
        this.id = id;
    }

    public InMemSupplier() {
        this.id = -1;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
