package me.yrf.ictse.jp.data.inmem.orders;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.data.IOrderManager;
import me.yrf.ictse.jp.api.data.entities.IOrder;
import me.yrf.ictse.jp.api.data.entities.IRequisition;
import me.yrf.ictse.jp.data.inmem.AbstractInMemIdBasedManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class InMemOrderManager
        extends AbstractInMemIdBasedManager<IOrder>
        implements IOrderManager {
    @Override
    public CompletableFuture<IOrder> approveReq(IRequisition req) {
        val future = new CompletableFuture<IOrder>();
        val reqMan = Registry.getInstance().getDbManager().getRequisitionManager();
        val ord = new InMemOrder(data.size());
        ord.setCreator(req.getCreator());
        ord.setDueDate(req.getDueDate());
        ord.setItems(req.getItems());
        ord.setApprover(Registry.getInstance().getCurrentUser().get());
        ord.setOrderedOn(null);

        doInsert(future, ord);
        reqMan.remove(req.getId());

        return future;
    }

    @Override
    public CompletableFuture<IOrder> add(IOrder item) {
        val future = new CompletableFuture<IOrder>();
        val ord = new InMemOrder(data.size());
        ord.setCreator(item.getCreator());
        ord.setDueDate(item.getDueDate());
        ord.setItems(item.getItems());
        ord.setApprover(item.getApprover());
        ord.setOrderedOn(item.getOrderedOn());

        doInsert(future, ord);

        return future;
    }

    public void save() throws IOException {
        val mapper = new ObjectMapper();
        mapper.writeValue(new File("orders.json"), this.data);
    }
}
