package me.yrf.ictse.jp.data.inmem.items;

import me.yrf.ictse.jp.api.data.entities.IItem;

public class InMemItem implements IItem {
    private final int id;
    private String name;
    private String desc;
    private int supplierId;

    public InMemItem() {
        this.id = -1;
    }

    InMemItem(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public int getSupplierId() {
        return supplierId;
    }

    @Override
    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }
}
