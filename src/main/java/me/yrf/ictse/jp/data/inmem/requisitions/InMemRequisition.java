package me.yrf.ictse.jp.data.inmem.requisitions;

import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.IUser;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.api.data.entities.IRequisition;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class InMemRequisition implements IRequisition {
    private final int id;
    private LocalDate dueDate;
    private String creator;
    private Map<Integer, Integer> items;

    protected InMemRequisition(int id) {
        this.id = id;
    }

    public InMemRequisition() {
        this.id = -1;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public IUser getCreator() {
        val userMan = Registry.getInstance().getDbManager().getUserManager();
        try {
            return userMan.get(creator).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCreator(IUser creator) {
        this.creator = creator.getUsername();
    }

    @Override
    public Map<IItem, Integer> getItems() {
        val itemMan = Registry.getInstance().getDbManager().getItemManager();
        val out = items.entrySet()
                .parallelStream()
                .collect(Collectors.toMap(e -> {
                    try {
                        return itemMan.get(e.getKey()).get();
                    } catch (InterruptedException | ExecutionException e1) {
                        e1.printStackTrace();
                        return null;
                    }
                }, Map.Entry::getValue));
        try {
            out.remove(null);
        }
        catch(NullPointerException e) {
            //Do Nothing Here
        }
        return out;
    }

    public void setItems(Map<IItem, Integer> items) {
        this.items = items.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey().getId(), Map.Entry::getValue));
    }
}
