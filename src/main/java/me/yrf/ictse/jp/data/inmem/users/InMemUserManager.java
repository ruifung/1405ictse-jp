package me.yrf.ictse.jp.data.inmem.users;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import me.yrf.ictse.jp.api.auth.*;
import me.yrf.ictse.jp.exceptions.DuplicateEntryException;
import me.yrf.ictse.jp.exceptions.InvalidOperationException;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class InMemUserManager implements IUserManager {
    private Map<String, IUser> users = new LinkedHashMap<>();

    @Override
    public CompletableFuture<IUser> get(String username) {
        val future = new CompletableFuture<IUser>();
        if (users.containsKey(username))
            future.complete(users.get(username));
        else
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    @Override
    public CompletableFuture<IUser> get(int id) {
        val future = new CompletableFuture<IUser>();
        for (IUser u : users.values()) {
            if (u.getId() == id) {
                future.complete(u);
                break;
            }
        }
        if (!future.isDone())
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    @Override
    public CompletableFuture<Void> remove(String username) {
        val future = new CompletableFuture<Void>();
        if (users.containsKey(username)) {
            users.remove(username);
            future.complete(null);
        } else
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    @Override
    public CompletableFuture<IUser> add(IUser newUser, String password) {
        InMemUser user = new InMemUser(users.size(), newUser.getUsername());
        user.setFullName(newUser.getFullName());
        user.setAccessLevel(newUser.getAccessLevel());
        user.setPassword(password);

        val future = new CompletableFuture<IUser>();
        if (users.containsKey(user.getUsername())) {
            future.completeExceptionally(new DuplicateEntryException());
        } else {
            user.setPassword(password);
            users.put(user.getUsername(), user);
            future.complete(user);
        }
        return future;
    }

    @Override
    public CompletableFuture<IUser> update(IUser user) throws InvalidOperationException {
        val future = new CompletableFuture<IUser>();
        if (users.containsKey(user.getUsername())) {
            users.put(user.getUsername(), user);
            future.complete(user);
        } else
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    @Override
    public IAuthenticator getAuthenticator() {
        return (u, p) -> {
            IUser user = null;
            try {
                user = this.get(u).get();
                if (user.checkPassword(p))
                    return new AuthResultSuccess(user);
                else
                    return new AuthResultFailure("Invalid Credentials");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                if (e.getCause() instanceof NoSuchElementException)
                    return new AuthResultFailure("Invalid Credentials");
                else
                    return new AuthResultFailure(e.getMessage());
            }
        };
    }

    @Override
    public CompletableFuture<Integer> count() {
        val future = new CompletableFuture<Integer>();
        future.complete(users.size());
        return future;
    }

    @Override
    public CompletableFuture<List<IUser>> getMany(int startIndex, int amount) {
        val future = new CompletableFuture<List<IUser>>();
        if (startIndex + amount > users.size())
            future.completeExceptionally(new IndexOutOfBoundsException());
        val keys = users.keySet().toArray(new String[users.size()]);
        val out = new ArrayList<IUser>(amount > users.size() ? users.size() : amount);
        val end = (startIndex + amount) > keys.length ? keys.length : startIndex + amount;
        for (int i = startIndex; i < end; i++) {
            out.add(users.get(keys[i]));
        }
        future.complete(out);
        return future;
    }

    public void save() throws IOException {
        val mapper = new ObjectMapper();
        mapper.writeValue(new File("users.json"), this.users);
    }
}
