package me.yrf.ictse.jp.data.inmem;

import me.yrf.ictse.jp.api.auth.IUserManager;
import me.yrf.ictse.jp.api.data.IDBManager;
import me.yrf.ictse.jp.data.inmem.items.InMemItemManager;
import me.yrf.ictse.jp.data.inmem.orders.InMemOrderManager;
import me.yrf.ictse.jp.data.inmem.requisitions.InMemRequisitionManager;
import me.yrf.ictse.jp.data.inmem.suppliers.InMemSupplierManager;
import me.yrf.ictse.jp.data.inmem.users.InMemUserManager;

import java.io.IOException;

public class InMemDbManager implements IDBManager {
    private final InMemUserManager userManager = new InMemUserManager();
    private final InMemItemManager itemManager = new InMemItemManager();
    private final InMemRequisitionManager requisitionManager = new InMemRequisitionManager();
    private final InMemOrderManager orderManager = new InMemOrderManager();
    private final InMemSupplierManager supplierManager = new InMemSupplierManager();

    @Override
    public IUserManager getUserManager() {
        return userManager;
    }

    @Override
    public InMemItemManager getItemManager() {
        return itemManager;
    }

    @Override
    public InMemRequisitionManager getRequisitionManager() {
        return requisitionManager;
    }

    @Override
    public InMemOrderManager getOrderManager() {
        return orderManager;
    }

    public InMemSupplierManager getSupplierManager() {
        return supplierManager;
    }

    @Override
    public void close() throws IOException {
        /**
        userManager.save();
        itemManager.save();
        requisitionManager.save();
        orderManager.save();
        supplierManager.save();
         **/
    }
}
