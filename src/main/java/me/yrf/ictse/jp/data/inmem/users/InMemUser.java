package me.yrf.ictse.jp.data.inmem.users;

import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.api.auth.IUser;

public class InMemUser implements IUser {

    private final String username;
    private final int id;
    private String fullName;
    private EnumUserAccessLevel accessLevel;
    private String password;

    public InMemUser(String username) {
        this(-1, username);
    }

    InMemUser(int id, String username) {
        this.username = username;
        this.id = id;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public EnumUserAccessLevel getAccessLevel() {
        return accessLevel;
    }

    @Override
    public void setAccessLevel(EnumUserAccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public boolean checkPassword(String password) {
        return this.password.equals(password);
    }

    @Override
    public boolean setPassword(String password) {
        this.password = password;
        return true;
    }
}
