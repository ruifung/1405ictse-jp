package me.yrf.ictse.jp.data.inmem.items;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import me.yrf.ictse.jp.api.data.IItemManager;
import me.yrf.ictse.jp.api.data.entities.IItem;
import me.yrf.ictse.jp.data.inmem.AbstractInMemIdBasedManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class InMemItemManager
        extends AbstractInMemIdBasedManager<IItem>
        implements IItemManager {

    @Override
    public CompletableFuture<IItem> add(IItem item) {
        val future = new CompletableFuture<IItem>();

        val newItem = new InMemItem(data.size());
        newItem.setName(item.getName());
        newItem.setDesc(item.getDesc());
        newItem.setSupplierId(item.getSupplierId());

        doInsert(future, newItem);

        return future;
    }

    public void save() throws IOException {
        val mapper = new ObjectMapper();
        mapper.writeValue(new File("items.json"), this.data);
    }
}
