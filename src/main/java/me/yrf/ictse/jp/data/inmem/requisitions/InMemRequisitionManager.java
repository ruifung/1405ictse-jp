package me.yrf.ictse.jp.data.inmem.requisitions;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import me.yrf.ictse.jp.api.data.IRequisitionManager;
import me.yrf.ictse.jp.api.data.entities.IRequisition;
import me.yrf.ictse.jp.data.inmem.AbstractInMemIdBasedManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class InMemRequisitionManager
        extends AbstractInMemIdBasedManager<IRequisition>
        implements IRequisitionManager {

    @Override
    public CompletableFuture<IRequisition> add(IRequisition item) {
        val future = new CompletableFuture<IRequisition>();
        val req = new InMemRequisition(data.size());
        req.setCreator(item.getCreator());
        req.setDueDate(item.getDueDate());
        req.setItems(item.getItems());

        doInsert(future, req);

        return future;
    }

    public void save() throws IOException {
        val mapper = new ObjectMapper();
        mapper.writeValue(new File("requisitions.json"), this.data);
    }
}
