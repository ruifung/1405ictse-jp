package me.yrf.ictse.jp.data.inmem.orders;

import lombok.val;
import me.yrf.ictse.jp.Registry;
import me.yrf.ictse.jp.api.auth.IUser;
import me.yrf.ictse.jp.api.data.entities.IOrder;
import me.yrf.ictse.jp.data.inmem.requisitions.InMemRequisition;

import java.time.ZonedDateTime;
import java.util.concurrent.ExecutionException;

public class InMemOrder extends InMemRequisition implements IOrder {

    private int approverId;
    private ZonedDateTime orderedOn;

    InMemOrder(int id) {
        super(id);
    }

    public InMemOrder() {
        super();
    }

    @Override
    public IUser getApprover() {
        val userMan = Registry.getInstance().getDbManager().getUserManager();
        try {
            return userMan.get(approverId).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setApprover(IUser user) {
        approverId = user.getId();
    }

    @Override
    public ZonedDateTime getOrderedOn() {
        return orderedOn;
    }

    @Override
    public void setOrderedOn(ZonedDateTime orderedOn) {
        this.orderedOn = orderedOn;
    }
}
