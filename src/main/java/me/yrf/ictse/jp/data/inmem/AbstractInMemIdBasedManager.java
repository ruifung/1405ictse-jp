package me.yrf.ictse.jp.data.inmem;

import lombok.val;
import me.yrf.ictse.jp.api.data.IIdManager;
import me.yrf.ictse.jp.api.data.entities.IIdEntity;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * This implementation assumes that ID's will be >= 0
 *
 * @param <T>
 */
public abstract class AbstractInMemIdBasedManager<T extends IIdEntity> implements IIdManager<T> {
    protected Map<Integer, T> data = new LinkedHashMap<>();

    @Override
    public CompletableFuture<T> get(int id) {
        val future = new CompletableFuture<T>();
        if (data.containsKey(id))
            future.complete(data.get(id));
        else
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    public CompletableFuture<T> update(T item) {
        val future = new CompletableFuture<T>();
        if (data.containsKey(item.getId()) && item.getId() >= 0) {
            data.put(item.getId(), item);
            future.complete(item);
        } else
            future.completeExceptionally(new IllegalArgumentException("Invalid item to update."));
        return future;
    }

    public CompletableFuture<T> remove(int id) {
        val future = new CompletableFuture<T>();
        if (data.containsKey(id))
            future.complete(data.remove(id));
        else
            future.completeExceptionally(new NoSuchElementException());
        return future;
    }

    @Override
    public CompletableFuture<Integer> count() {
        val future = new CompletableFuture<Integer>();
        future.complete(data.size());
        return future;
    }

    @Override
    public CompletableFuture<List<T>> getMany(int startIndex, int amount) {
        val future = new CompletableFuture<List<T>>();

        if (startIndex + amount > data.size()) {
            future.completeExceptionally(new IndexOutOfBoundsException());
            return future;
        }

        val keys = data.keySet().toArray(new Integer[data.size()]);
        val end = startIndex + amount;
        val out = new LinkedList<T>();
        for (int i = startIndex; i < end; i++) {
            out.add(data.get(keys[i]));
        }
        future.complete(out);
        return future;
    }

    protected void doInsert(CompletableFuture<T> future, T item) {
        if (item.getId() >= 0 && data.containsKey(item.getId()))
            future.completeExceptionally(new IllegalArgumentException("Cannot add an existing item!"));
        else if (item.getId() < 0)
            future.completeExceptionally(new IllegalStateException("ID may not be below 0"));
        else {
            data.put(item.getId(), item);
            future.complete(item);
        }
    }
}
