package me.yrf.ictse.jp.data.inmem.suppliers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import me.yrf.ictse.jp.api.data.ISupplierManager;
import me.yrf.ictse.jp.api.data.entities.ISupplier;
import me.yrf.ictse.jp.data.inmem.AbstractInMemIdBasedManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class InMemSupplierManager
        extends AbstractInMemIdBasedManager<ISupplier>
        implements ISupplierManager {
    @Override
    public CompletableFuture<ISupplier> add(ISupplier item) {
        val future = new CompletableFuture<ISupplier>();
        val out = new InMemSupplier(data.size());
        out.setName(item.getName());
        out.setAddress(item.getAddress());
        out.setEmail(item.getEmail());
        out.setPhone(item.getPhone());

        doInsert(future, out);

        return future;
    }

    public void save() throws IOException {
        val mapper = new ObjectMapper();
        mapper.writeValue(new File("requisitions.json"), this.data);
    }
}
