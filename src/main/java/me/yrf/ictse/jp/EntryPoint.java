package me.yrf.ictse.jp;

import com.jfoenix.controls.JFXDialog;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.val;
import me.yrf.ictse.jp.api.auth.EnumUserAccessLevel;
import me.yrf.ictse.jp.api.auth.IUser;
import me.yrf.ictse.jp.data.inmem.InMemDbManager;
import me.yrf.ictse.jp.data.inmem.users.InMemUser;
import me.yrf.ictse.jp.exceptions.DBException;
import me.yrf.ictse.jp.exceptions.InvalidOperationException;
import me.yrf.ictse.jp.gui.StyleRegistry;
import me.yrf.ictse.jp.gui.activities.LoginActivity;
import me.yrf.ictse.jp.gui.activities.SetupWizard;
import me.yrf.ictse.jp.gui.dialogs.LoadingDialog;
import me.yrf.ictse.jp.gui.dialogs.MessageDialog;
import org.kairos.components.MaterialButton;
import org.kairos.core.ActivityFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class EntryPoint extends Application {
    private static int exitCode = 0;
    private static boolean dbError = false;

    public static void main(String[] args) {
        launch(args);
        System.exit(exitCode);
    }

    /**
     * Preinitialization method
     * <p/>
     * Used to perform initialization before starting the
     * JavaFX components. e.g. Database initialization.
     * <p/>
     * This method should only be called once per execution.
     * As it is called during startup, the setter for dbManager
     * will not throw a InvalidOperationException.
     *
     * @param args args passed to the program.
     */
    @SneakyThrows(InvalidOperationException.class)
    private static void preinit(Parameters args) throws DBException {
        Registry.getInstance().setDbManager(new InMemDbManager());
        /**
        //TODO: Remove from release;
        val users = Registry.getInstance().getDbManager().getUserManager();
        IUser user = new InMemUser("admin");
        user.setFullName("Administrator");
        user.setAccessLevel(EnumUserAccessLevel.ADMINISTRATOR);
        users.add(user, "admin");
        user = new InMemUser("manager");
        user.setFullName("Manager");
        user.setAccessLevel(EnumUserAccessLevel.MANAGER);
        users.add(user, "manager");
        user = new InMemUser("staff");
        user.setFullName("Staff");
        user.setAccessLevel(EnumUserAccessLevel.STAFF);
        users.add(user, "staff");
         **/
    }

    public static void exit(int code) {
        exitCode = code;
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("POPS System");
        Registry.getInstance().getCurrentUserProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isPresent())
                try {
                    startApplication(primaryStage, new StackPane(), false);
                } catch (IOException e) {
                    MaterialButton button = new MaterialButton();
                    button.setText("Exit");
                    button.setOnAction((a) -> {
                        exitCode = 1;
                        Platform.exit();
                    });
                    errorDialog("Error Loading GUI", e,
                            (Pane) primaryStage.getScene().getRoot(),
                            button
                    );
                }
        });
        startApplication(primaryStage, new StackPane(), true);
        primaryStage.maximizedProperty().addListener((ob, ov, nv) -> {
            Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
            if (nv) {
                primaryStage.setX(bounds.getMinX());
                primaryStage.setY(bounds.getMinY());
                primaryStage.setHeight(bounds.getHeight());
                primaryStage.setWidth(bounds.getWidth());
            }
        });
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);
        primaryStage.show();

    }

    @Override
    public void stop() throws Exception {
        try {
            if (!dbError)
                Registry.getInstance().getDbManager().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.stop();
    }

    public static MessageDialog errorDialog(String title, Throwable t, Pane root, Button closeBtn) {
        try {
            MessageDialog dialog = new MessageDialog(root, JFXDialog.DialogTransition.CENTER);
            dialog.setTitle(title);
            dialog.setText(t.getMessage());
            dialog.setIcon(new Image(EntryPoint.class.getResourceAsStream("/assets/icons/error_exclaimation.png")));
            dialog.setOverlayCloseable(false);
            dialog.show();
            dialog.getButtonBox().getChildren().add(closeBtn);
            return dialog;
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    private void startApplication(Stage primaryStage, Pane root, boolean init) throws IOException {
        primaryStage.setScene(new Scene(root));
        primaryStage.getScene().getStylesheets().add(
                MaterialButton.class.getResource("controls.css").toExternalForm());
        StyleRegistry.applyDefaultStyles(primaryStage.getScene());

        ActivityFactory factory = new ActivityFactory(primaryStage);
        val loading = new LoadingDialog(root, JFXDialog.DialogTransition.CENTER);
        //Perform initialization functions asynchronously, so the UI doesn't block.
        ForkJoinPool.commonPool().submit(() -> {
            try {
                if (init)
                    preinit(getParameters());
                Platform.runLater(() -> {
                    loading.close();
                    Registry registry = Registry.getInstance();
                    try {
                        if (registry.getDbManager().getUserManager().count().get() <= 0)
                            factory.startActivity(SetupWizard.class);
                        else
                            factory.startActivity(LoginActivity.class);
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        MaterialButton button = new MaterialButton();
                        button.setText("Exit");
                        button.setOnAction((a) -> {
                            exitCode = 1;
                            dbError = true;
                            Platform.exit();
                        });
                        errorDialog("Error", e, root, button);
                        loading.close();
                    }
                });
            } catch (Throwable t) {
                Platform.runLater(() -> {
                    t.printStackTrace();
                    MaterialButton button = new MaterialButton();
                    button.setText("Exit");
                    button.setOnAction((a) -> {
                        exitCode = 1;
                        dbError = true;
                        Platform.exit();
                    });
                    errorDialog("Database Error", t, root, button);
                    loading.close();
                });
            }
        });
        loading.setOverlayCloseable(false);
        loading.show();
    }
}
